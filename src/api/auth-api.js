import {createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut} from "firebase/auth";
import {auth} from "../firebase-config";
import { API_BASE_URL } from "../config";
import axios from "axios";


function login(user){
    const {email, password} = user;
    return signInWithEmailAndPassword(auth, email, password);
}

function logout() {
    return signOut(auth);
}

function register(user) {
    const {email, password} = user;
    return createUserWithEmailAndPassword(auth, email, password);
}

function signup (user){
    return axios.post(`${API_BASE_URL}/auth/signup`, user);
}

function signin (user){
    return axios.post(`${API_BASE_URL}/auth/login`, user);
}

function getMe (){
    const token = localStorage.getItem("token");
    const headers = {
        'Authorization': `Bearer ${token}`
    }
    return axios.get(`${API_BASE_URL}/auth/me`, {headers});
}

function signout (user){
    localStorage.setItem('token', null);
}




export { login, logout, register, signup, signin, getMe, signout };