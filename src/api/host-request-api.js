import axios from "axios";
import { API_BASE_URL } from "../config";

const endpoint = `${API_BASE_URL}/host_requests`;

async function add(body) {
    return axios.post(endpoint, body);
}

async function getAllRequests(params) {
    return axios.get(endpoint, {params});
}

async function getOne(id) {
    return axios.get(`${endpoint}/${id}`);
}

async function update(id, body) {
    return axios.patch(`${endpoint}/${id}`, body);
}

export { add, getAllRequests, getOne, update };