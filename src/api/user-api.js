import { auth, db } from "../firebase-config";
import { updatePassword, reauthenticateWithCredential, EmailAuthProvider } from 'firebase/auth';
import axios from "axios";
import { API_BASE_URL } from "../config";


function addUser(user) {
    return axios.post(`${API_BASE_URL}/users`, user);
}

function getUser(uid) {
    return axios.get(`${API_BASE_URL}/current/${uid}`);
}

function updateUser(id, user) {
    return axios.patch(`${API_BASE_URL}/users/${id}`, user);
}

function getAll(params) {
    return axios.get(`${API_BASE_URL}/users`, {params});
}

function deleteUser(id) {
    return axios.delete(`${API_BASE_URL}/users/${id}`);
}


async function resetPassword(newPassword, {email, password}) {
    const user = auth.currentUser;
    const localCredentials = EmailAuthProvider.credential(email, password);
    await reauthenticateWithCredential(user, localCredentials);
    return updatePassword(user, newPassword);
}

export { resetPassword, addUser, getUser, updateUser, getAll, deleteUser };