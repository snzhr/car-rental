import axios from "axios";
import { API_BASE_URL } from "../config";

const endpoint = `${API_BASE_URL}/rentals`;

async function add(rental) {
    return axios.post(endpoint, rental);
}

async function getAll(params) {
    return axios.get(endpoint, {params});
}

async function update(id, body) {
    return axios.patch(`${endpoint}/${id}`, body);
}

async function remove(id) {
    return axios.delete(`${endpoint}/${id}`);
}


export { add, getAll, update, remove };