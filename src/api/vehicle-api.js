import axios from "axios";
import { API_BASE_URL } from "../config";


async function add(vehicle) {
    return axios.post(`${API_BASE_URL}/vehicles`, vehicle);
}

async function getAll(params) {
    return axios.get(`${API_BASE_URL}/vehicles`, {params});
}

async function getOne(id) {
    return axios.get(`${API_BASE_URL}/vehicles/${id}`);
}

async function update(id, vehicle) {
    return axios.patch(`${API_BASE_URL}/vehicles/${id}`, vehicle);
}

async function remove(id) {
    return axios.delete(`${API_BASE_URL}/vehicles/${id}`);
}

async function getAvailable(params) {
    return axios(`${API_BASE_URL}/all_rentals`, {params});
}

export { add, getOne, getAll, update, remove, getAvailable };