import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage } from "../firebase-config";
export async function read(file, cb) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
        cb(reader.result);
    };
}

export async function upload(file){
    const filePath = `user-images/${Date.now()}-${file.name}`;
    const storageRef = ref(storage, filePath);
    const uploaded = await uploadBytes(storageRef, file);
    const fileUrl = await getDownloadURL(uploaded.ref);
    return fileUrl;
}