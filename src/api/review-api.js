import axios from "axios";
import { API_BASE_URL } from "../config";

function add(review) {
    return axios.post(`${API_BASE_URL}/reviews`, review);
}

function getAll(params) {
    return axios.get(`${API_BASE_URL}/reviews`, {params});
}

function remove(id) {
    return axios.delete(`${API_BASE_URL}/reviews/${id}`, );
}



export { add, getAll, remove };