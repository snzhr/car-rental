import { API_BASE_URL } from "../config";
import axios from "axios";

const endpoint = `${API_BASE_URL}/statistics`;

async function getStats() {
    return axios.get(endpoint);
}

export { getStats };