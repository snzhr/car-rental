import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    user: null,
    userState: false,
    token: null,
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        add(state, action) {
            state.user = action.payload;
        },
        setUserState(state, action) {
            state.userState = action.payload;
        },
        setToken(state, action){
            state.token = action.payload;
        }
    },
})

export const { add, setUserState, setToken } = userSlice.actions;

export default userSlice.reducer;