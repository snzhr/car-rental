import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    loaderState: false,
}

export const loaderSlice = createSlice({
    name: 'loader',
    initialState,
    reducers: {
        setLoaderState(state, action) {
            state.loaderState = action.payload;
        },
    },
})

export const {setLoaderState} = loaderSlice.actions;

export default loaderSlice.reducer;