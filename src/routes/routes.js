import React from "react";
import { createBrowserRouter } from "react-router-dom";
import Home from "../app/views/home/Home";
import { AdminHome } from "../app-admin/views/admin-home/AdminHome";
import { Dashboard } from "../app-admin/views/dashboard/Dashboard";
import { Login } from "../app/views/login/Login";
import { Register } from "../app/views/register/Register";
import App from "../App";
import { Profile } from "../app/views/profile/Profile";
import { Main } from "../app/views/main/Main";
import { ProfileSettings } from "../app/views/profile/profile-settings/ProfileSettings";
import { ChangePassword } from "../app/views/profile/change-password/ChangePassword";
import { PaymentMethod } from "../app/views/profile/payment-method/PaymentMethod";
import NotFound from "../app/views/notfound/NotFound";
import { Vehicles } from "../app-admin/views/vehicles/Vehicles";
import { AddCar } from "../app-admin/views/add-car/AddCar";
import { UpdateCar } from "../app-admin/views/update-car/UpdateCar";
import { Reservations } from "../app-admin/views/reservations/Reservations";
import { AllVehicles } from "../app/views/all-vehicles/AllVehicles";
import { SingleVehicle } from "../app/views/single-vehicle/SingleVehicle";
import { MyRentals } from "../app/views/profile/my-rentals/MyRentals";
import { Customers } from "../app-admin/views/customers/Customers";
import { Settings } from "../app-admin/views/settings/Settings";
import { HostRequests } from "../app-admin/views/host-requests/HostRequests";
import { MyVehicles } from "../app/views/profile/my-vehicles/MyVehicles";
import { AddHostCar } from "../app/views/add-host-car/AddHostCar";
import { UpdateHostCar } from "../app/views/update-host-car/UpdateHostCar";

const router = createBrowserRouter([
    {
        path: "/",
        element: <App/>,
        children: [
            {
                path: '',
                element: <Main/>,
                children: [
                    {
                        path: '',
                        element: <Home/>
                    },
                    {
                        path: 'login',
                        element: <Login/>
                    },
                    {
                        path: 'register',
                        element: <Register/>
                    },
                    {
                        path: 'all-vehicles',
                        element: <AllVehicles/>
                    },
                    {
                        path: 'all-vehicles/:id',
                        element: <SingleVehicle/>
                    },
                    {
                        path: 'host/add-vehicle',
                        element: <AddHostCar/>
                    },
                    {
                        path: 'host/update-vehicle/:id',
                        element: <UpdateHostCar/>
                    },
                    {
                        path: 'profile',
                        element: <Profile/>,
                        children: [
                            {
                                path: '',
                                element: <ProfileSettings/>
                            },
                            {
                                path: 'my-rentals',
                                element: <MyRentals/>
                            },
                            {
                                path: 'payment-method',
                                element: <PaymentMethod/>
                            },
                            {
                                path: 'change-password',
                                element: <ChangePassword/>
                            },
                            {
                                path: 'my-vehicles',
                                element: <MyVehicles/>
                            }
                        ]
                    }
                ]
            },
            {
                path: "admin",
                element: <AdminHome/>,
                children: [
                    {
                        path: 'dashboard',
                        element: <Dashboard/>,
                        icon: 'pi pi-home',
                        label: 'Dashboard',
                        isMain: true
                    },
                    {
                        path: 'vehicles',
                        element: <Vehicles/>,
                        icon: 'pi pi-car',
                        label: 'Vehicles',
                        isMain: true
                    },
                    {
                        path: 'add-car',
                        element: <AddCar/>,
                        icon: 'pi pi-car',
                        label: 'Add car',
                        isMain: false
                    },
                    {
                        path: 'update-car/:id',
                        element: <UpdateCar/>,
                        icon: 'pi pi-car',
                        label: 'Update car',
                        isMain: false
                    },
                    {
                        path: 'reservations',
                        element: <Reservations/>,
                        icon: 'pi pi-car',
                        label: 'Reservations',
                        isMain: true
                    },
                    {
                        path: 'customers',
                        element: <Customers/>,
                        icon: 'pi pi-users',
                        label: 'Customers',
                        isMain: true
                    },
                    {
                        path: 'host-requests',
                        element: <HostRequests/>,
                        icon: 'pi pi-user-plus',
                        label: 'Hosts',
                        isMain: true
                    },
                    {
                        path: 'settings',
                        element: <Settings/>,
                        icon: 'pi pi-cog',
                        label: 'Settings',
                        isMain: true
                    }
                ]
            },
            {
                path: "*",
                element: <NotFound/>,
            },
        ]
    },
]);

export default router;