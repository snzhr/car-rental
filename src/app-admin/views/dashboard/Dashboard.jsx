import styles from './Dashboard.module.css';
import { StatisticsBlock } from "../../components/statistics-block/StatisticsBlock";
import { useEffect, useState } from "react";
import { getAll } from "../../../api/rent-api";
import { getStats } from "../../../api/stats-api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { RentalStatusTag } from "../../../shared/ui/rental-status-tag/RentalStatusTag";
import { PieChart } from "../../../shared/ui/pie-chart/PieChart";
import { LineChart } from "../../../shared/ui/line-chart/LineChart";

export const Dashboard = () => {
    const [resentRentals, setResentRentals] = useState([]);
    const [stats, setStats] = useState([]);
    const [loading, setLoading] = useState(false);

    async function getRentals() {
        try {
            setLoading(true);
            const res = await getAll({page: 1, perPage: 5});
            setResentRentals(res.data.items);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    async function getStatistics() {
        try {
            setLoading(true);
            const res = await getStats();
            setStats(res.data);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getRentals();
        getStatistics();
    }, []);

    const statusBodyTemplate = (product) => {
        return <RentalStatusTag status={product.status}/>;
    };

    return (
        <div className={styles.dashboard}>
            <div className="header flex gap-6">
                {
                    stats.map((data, index) => <StatisticsBlock key={index} {...data} />)
                }
            </div>
            <div className={styles.bookings}>
                <h1 className="text-gray-600 font-bold">Recent vehicle bookings</h1>

                <DataTable value={resentRentals} loading={loading} tableStyle={{minWidth: '50rem'}}>
                    <Column body={data => `${data.rental_vehicle.make} ${data.rental_vehicle.model}`}
                            header="Vehicle"></Column>
                    <Column body={(data) => (data.start_date) ? (data.start_date) : '-'} header="Start date"></Column>
                    <Column body={(data) => (data.end_date) ? (data.end_date) : '-'} header="End date"></Column>
                    <Column body={statusBodyTemplate} header="Status"></Column>
                    <Column body={data => `$${data.total_cost}`} header="Total cost"></Column>
                </DataTable>
            </div>

            <div className="charts flex gap-6">
                <div
                    className="w-1/3 font-bold text-gray-600 h-auto flex flex-col items-center p-5 rounded-2xl border border-gray-400"
                    style={{boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px'}}>
                    <h1>Vehicle reservation statistics</h1>
                    <PieChart/>
                </div>
                <div className="w-full font-bold text-gray-600 p-5 rounded-2xl scroll-auto border border-gray-400"
                     style={{boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px'}}>
                    <h1 className="text-center">Income statistics</h1>
                    <LineChart/>
                </div>
            </div>
        </div>
    )
}