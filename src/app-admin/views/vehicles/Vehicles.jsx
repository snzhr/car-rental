import styles from './Vehicles.module.css';
import { CarCard } from "../../../shared/ui/car-card/CarCard";
import { useNavigate } from "react-router-dom";
import { PageHeader } from "../../components/page-header/PageHeader";
import { useEffect, useState } from "react";
import { getAll, remove } from "../../../api/vehicle-api";
import { ProgressSpinner } from "primereact/progressspinner";
import { confirmDialog, ConfirmDialog } from "primereact/confirmdialog";
import { Pagination } from "../../../shared/ui/pagination/Pagination";
import { showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";


export const Vehicles = () => {
    const router = useNavigate();
    const [vehicles, setVehicles] = useState([]);
    const [loading, setLoading] = useState(false);
    const [paging, setPaging] = useState({
        items: 0,
        page: 1,
        perPage: 10,
    });
    const [query, setQuery] = useState({...paging});

    async function getVehicles(){
        try {
            setLoading(true);
            const res = await getAll(query);
            setPaging({...paging, items: res.data.itemsTotal});
            setVehicles(res.data.items);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getVehicles();
    }, [query]);


    const handleEditVehicle = (id) => {
        router(`/admin/update-car/${id}`);
    }

    const handlePageChange = (e) => {
        setQuery({...query, page: e.page + 1, perPage: e.rows});
    }

    const handleRemove = async (id) => {
        try {
            await remove(id);
            await getVehicles();
            showSuccess(toastRef, "Vehicle successfully deleted");
        }catch (e) {
            console.log(e);
        }
    }

    const confirm = (data) => {
        confirmDialog({
            message: 'Do you want to delete this vehicle?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            defaultFocus: 'reject',
            acceptClassName: 'p-button-danger',
            accept: async () => {
                await handleRemove(data);
            },
        });
    };

    return (
        <div className={styles.vehicles}>
            {loading ?
                <div className="card flex justify-content-center items-center h-80">
                    <ProgressSpinner/>
                </div>
                : <>
                    <PageHeader btnClick={() => router('/admin/add-car')} hasBtn header="All vehicles"/>
                    <div className={styles.content}>
                        {
                            vehicles.map(vehicle => {
                                return <CarCard key={vehicle.id} car={vehicle} handleDeleteClick={confirm} handleEdit={handleEditVehicle}/>
                            })
                        }
                    </div>
                </>
            }
            <Pagination totalItems={paging.items} onPageClick={handlePageChange}/>
            <ConfirmDialog />
        </div>
    )
}