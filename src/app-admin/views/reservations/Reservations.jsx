import { useEffect, useState } from "react";
import { getAll, remove, update } from "../../../api/rent-api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { PageHeader } from "../../components/page-header/PageHeader";
import { Pagination } from "../../../shared/ui/pagination/Pagination";
import { RentalStatusTag } from "../../../shared/ui/rental-status-tag/RentalStatusTag";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { showError, showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";

export const Reservations = () => {
    const [recentRentals, setRecentRentals] = useState([]);
    const [loading, setLoading] = useState(false);
    const [statuses] = useState(['ongoing', 'cancelled', 'returned']);
    const [pagination, setPagination] = useState({
        items: 0,
        page: 1,
        perPage: 5,
    });
    const [query, setQuery] = useState(pagination);

    async function getRentals() {
        try {
            setLoading(true);
            const res = await getAll(query);
            setRecentRentals(res.data.items);
            setPagination({...pagination, items: res.data.itemsTotal});
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getRentals();
    }, [query]);

    const handlePageChange = (e) => {
        setQuery({...query, page: e.page + 1, perPage: e.rows});
    }

    const imageBodyTemplate = (data) => {
        return <img src={data.rental_vehicle.imageUrl} alt="img" className="w-full shadow-2 border-round" />;
    };

    const statusBodyTemplate = (product) => {
        return <RentalStatusTag status={product.status} />;
    };

    async function handleEdit({data, index, newData}){
        try {
            const modified = {
                ...newData,
                start_date: null,
                end_date: null,
            }
            const _rentals = [...recentRentals];
            await update(data.id, modified);
            _rentals[index] = modified;
            setRecentRentals(_rentals);
        }catch (e){
            console.log(e);
        }
        finally {

        }
    }

    const statusEditor = (options) => {
        return (
            <Dropdown
                value={options.value}
                options={statuses}
                onChange={(e) => options.editorCallback(e.value)}
                placeholder="Select a status"
                itemTemplate={(option) => {
                    return <RentalStatusTag status={option} />
                }}
            />
        );
    };

    async function removeRental(id){
        try {
            await remove(id)
            await getRentals();
            showSuccess(toastRef, "Reservation successfully deleted");
        }catch (e) {
            showError(toastRef, "Something went wrong. Try again later.");
        }
    }

    const confirm = (data) => {
        confirmDialog({
            message: 'Do you want to delete this reservation?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            defaultFocus: 'reject',
            acceptClassName: 'p-button-danger',
            accept: async () => {
                await removeRental(data.id);
            },
        });
    };

    const actionsBodyTemplate = (data) => {
        return <Button onClick={() => confirm(data)} icon="pi pi-trash" severity="danger" tooltip="Delete customer" tooltipOptions={{position: 'top'}} rounded outlined aria-label="Delete" />
    };

    return (
        <div className="flex flex-col gap-6">
            <PageHeader header="All vehicle bookings"/>
            <div className="min-w-fit">
                <DataTable
                    value={recentRentals}
                    loading={loading}
                    editMode="row"
                    onRowEditComplete={handleEdit}
                >
                    {/*editor={(options) => textEditor(options)}*/}
                    <Column field="id" header="ID"></Column>
                    <Column body={data => `${data.user.name} ${data.user.lastname}`} header="Customer"></Column>
                    <Column
                        body={data => `${data.rental_vehicle.make} ${data.rental_vehicle.model}`}
                        header="Vehicle">
                    </Column>
                    <Column body={imageBodyTemplate} header="Vehicle image"
                            bodyClassName="w-32"
                    ></Column>
                    <Column field="status" body={statusBodyTemplate} header="Status" editor={statusEditor}></Column>
                    <Column body={(data) => (data.start_date) ? (data.start_date) : '-'} header="Start date"></Column>
                    <Column body={(data) => (data.end_date) ? (data.end_date) : '-'} header="End date"></Column>
                    <Column body={data => `$${data.total_cost}`} header="Total cost"></Column>
                    <Column rowEditor header="Edit"></Column>
                    <Column body={actionsBodyTemplate} header="Actions"></Column>
                </DataTable>
                <Pagination totalItems={pagination.items} onPageClick={handlePageChange}/>
            </div>
            <ConfirmDialog />
        </div>
    )
}