import { PageHeader } from "../../components/page-header/PageHeader";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Pagination } from "../../../shared/ui/pagination/Pagination";
import { useEffect, useState } from "react";
import { deleteUser, getAll } from "../../../api/user-api";
import user_profile_fallback from '../../../assets/images/user_profile_fallback.jpg';
import { Button } from "primereact/button";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { showError, showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";

export const Customers = () => {
    const [customers, setCustomers] = useState([]);
    const [loading, setLoading] = useState(false);
        const [pagination, setPagination] = useState({
        items: 0,
        page: 1,
        perPage: 5,
    });
    const [query, setQuery] = useState(pagination);

    async function getCustomers() {
        try {
            setLoading(true);
            const res = await getAll(query);
            setCustomers(res.data.items);
            setPagination({...pagination, items: res.data.itemsTotal});
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getCustomers();
    }, [query]);

    const handlePageChange = (e) => {
        setQuery({...query, page: e.page + 1, perPage: e.rows});
    }

    async function removeUser(id){
        try {
            await deleteUser(id);
            await getCustomers();
            showSuccess(toastRef, "User successfully deleted");
        }catch (e) {
            showError(toastRef, "Something went wrong. Try again later.");
        }
    }

    const confirm = (data) => {
        confirmDialog({
            message: 'Do you want to delete this customer?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            defaultFocus: 'reject',
            acceptClassName: 'p-button-danger',
            accept: async () => {
                await removeUser(data.id);
            },
        });
    };

    const imageBodyTemplate = (data) => {
        return <img src={data.profileImg || user_profile_fallback} alt="img" className="w-full shadow-2 border-round" />;
    };

    const actionsBodyTemplate = (data) => {
        return <Button onClick={() => confirm(data)} icon="pi pi-trash" severity="danger" tooltip="Delete customer" tooltipOptions={{position: 'top'}} rounded outlined aria-label="Delete" />
    };

    return (
        <div className="flex flex-col gap-6">
            <PageHeader header="All customers"/>
            <div className="min-w-fit">
                <DataTable
                    value={customers}
                    loading={loading}
                >
                    <Column field="id" header="ID"></Column>
                    <Column field="name" header="Name"></Column>
                    <Column field="lastname" header="Surname"></Column>
                    <Column body={imageBodyTemplate} header="Profile image" bodyClassName="w-24"></Column>
                    <Column field="email" header="Email"></Column>
                    <Column body={data => data.phone || 'No data'} header="Phone"></Column>
                    <Column body={actionsBodyTemplate} header="Actions"></Column>
                </DataTable>
                <Pagination totalItems={pagination.items} onPageClick={handlePageChange}/>
            </div>
            <ConfirmDialog />
        </div>
    )
}