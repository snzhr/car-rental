import { PageHeader } from "../../components/page-header/PageHeader";
import { getAllRequests, update } from "../../../api/host-request-api";
import { useEffect, useState } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { Tag } from "primereact/tag";
import { showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { Dialog } from "primereact/dialog";
import { remove } from "../../../api/vehicle-api";

export const HostRequests = () => {
    const [requests, setRequests] = useState([]);
    const [loading, setLoading] = useState(false);
    const [approveLoading, setApproveLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const [requestedCar, setRequestedCar] = useState(null);

    async function getRequests() {
        try {
            setLoading(true);
            const res = await getAllRequests();
            setRequests(res.data);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getRequests();
    }, []);

    async function approve(data) {
        try {
            await update(data.id, {...data, isApproved: 'approved'});
            await getRequests();
            showSuccess(toastRef, "Request successfully approved.");
        } catch (e) {
            console.log(e);
        }
    }

    async function reject(data) {
        try {
            await update(data.id, {...data, isApproved: 'rejected'});
            await getRequests();
            showSuccess(toastRef, "Request successfully rejected.");
        } catch (e) {
            console.log(e);
        }
    }

    const confirm = (data) => {
        confirmDialog({
            message: 'Do you want to approve this request?',
            header: 'Action Confirmation',
            icon: 'pi pi-check-circle',
            defaultFocus: 'accept',
            acceptClassName: 'p-button-success',
            accept: async () => {
                await approve(data);
            },
        });
    };

    const confirm2 = (data) => {
        confirmDialog({
            message: 'Do you want to reject this request?',
            header: 'Action Confirmation',
            icon: 'pi pi-check-circle',
            defaultFocus: 'accept',
            acceptClassName: 'p-button-danger',
            accept: async () => {
                await reject(data);
            },
        });
    };

    const actionsBodyTemplate = (data) => {
        return <div className="flex gap-2">
            <Button onClick={() => confirm(data)} icon="pi pi-check" severity="success" tooltip="Approve request"
                    tooltipOptions={{position: 'top'}} rounded outlined aria-label="Accept"/>
            <Button onClick={() => confirm2(data)} icon="pi pi-ban" severity="danger" tooltip="Reject request"
                    tooltipOptions={{position: 'top'}} rounded outlined aria-label="Delete"/>
        </div>
    };

    const getSeverity = (status) => {
        switch (status) {
            case 'approved':
                return 'success';
            case 'pending':
                return 'warning';
            case 'rejected':
                return 'danger';
            default:
                return null;
        }
    };

    const statusBodyTemplate = (data) => {
        return <Tag severity={getSeverity(data.isApproved)} value={data.isApproved}></Tag>
    };


    const removalRequestTemplate = (data) => {
        return <div>
            {
                data.removal_requests.vehicle_id === 0 ? <span>No requests</span>
                    :
                    <span>Host requested to remove car: <Button onClick={() => {
                        setVisible(true);
                        setRequestedCar(data.removal_requests?.car_for_remove);
                    }} className="text-blue-700 underline" unstyled label="view" /></span>
            }
        </div>
    };

    const removeRequestedCar = async (vehicle) => {
        try {
            if (!vehicle){
                return
            }
            const hostReq = requests.find(item => item.users_id === vehicle.host_id);
            await remove(vehicle.id);
            await update(hostReq?.id, {...hostReq, removal_requests: {vehicle_id: 0}});
            await getRequests();
            showSuccess(toastRef, "Vehicle was successfully removed.");
            setVisible(false);
        }catch (e) {
            console.log(e);
        }
    }

    return (
        <div className="flex flex-col gap-6">
            <PageHeader header="All host requests"/>
            <div className="min-w-fit">
                <DataTable
                    value={requests}
                    loading={loading}
                >
                    <Column field="id" header="ID"></Column>
                    <Column body={data => `${data?.host_user?.name} ${data?.host_user?.lastname}`}
                            header="Host user"></Column>
                    <Column field="host_user.email" header="Email"></Column>
                    <Column body={removalRequestTemplate} header="Removal requests"></Column>
                    <Column field="isApproved" body={statusBodyTemplate} header="Status"></Column>
                    <Column body={actionsBodyTemplate} header="Actions"></Column>
                </DataTable>
            </div>
            <ConfirmDialog/>
            <Dialog header="Removal request" visible={visible} style={{ width: '50vw' }} onHide={() => setVisible(false)}>
                <div className="flex justify-around items-center">
                    <img src={requestedCar?.imageUrl}  className="w-1/2"/>
                    <div className="flex flex-col font-bold text-lg">
                        <h1>{requestedCar?.make} {requestedCar?.model}</h1>
                        <h1>{requestedCar?.color.name}</h1>
                        <h1>{requestedCar?.type.name}</h1>
                    </div>
                </div>
                <Button severity="danger" label="Remove" onClick={() => removeRequestedCar(requestedCar)}/>
            </Dialog>
        </div>
    )
}