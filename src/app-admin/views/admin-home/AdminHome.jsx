import styles from './AdminHome.module.css'
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import {Header} from "../../components/header/Header";
import {SidePanel} from "../../components/side-panel/SidePanel";
import {useEffect} from "react";
import { useSelector } from "react-redux";

export const AdminHome = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const {user} = useSelector(state => state.user);
    useEffect(() => {
        if (location.pathname.split('/').length === 2 && location.pathname.split('/')[1] === 'admin') {
            navigate('/admin/dashboard');
        }
    });

    useEffect(() => {
        if (!user?.isAdmin){
            navigate('/');
        }
    }, [user]);
    return (
        <div className={styles.main}>
                <SidePanel/>

            <div className={styles.content}>
                <Header/>
                <div className={styles.outlet}>
                    <Outlet/>
                </div>
            </div>
        </div>
    )
}