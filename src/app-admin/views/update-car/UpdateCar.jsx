import styles from './UpdateCar.module.css';
import { PageHeader } from "../../components/page-header/PageHeader";
import { CarForm } from "../../components/car-form/CarForm";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { update, getOne } from "../../../api/vehicle-api";
import { upload } from "../../../api/file-api";
import { showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { vehicleMessages } from "../../../shared/messages";

export const UpdateCar = () => {
    const {id} = useParams();
    const [car, setCar] = useState(null);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        async function fetchVehicle() {
            try {
                const response = await getOne(id);
                setCar(response.data);
            } catch (e) {
                console.log(e);
            }
        }

        fetchVehicle();
    }, []);

    const handleAddCar = async ({file, car}) => {
        try {
            setLoading(true);
            let imageUrl = car.imageUrl;
            if (file){
                imageUrl = await upload(file);
            }
            await update(car.id, {...car, imageUrl});
            showSuccess(toastRef, vehicleMessages['vehicle-updated-successfully']);
        } catch (e) {
            console.log(e);
        }finally {
            setLoading(false);
        }
    }

    return (
        <div className={styles.main}>
            <PageHeader header="Update car"/>
            <CarForm vehicle={car} onSave={handleAddCar} loader={loading}/>
        </div>
    )
}