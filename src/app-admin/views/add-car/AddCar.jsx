import styles from './AddCar.module.css';
import { CarForm } from "../../components/car-form/CarForm";
import { PageHeader } from "../../components/page-header/PageHeader";
import { upload } from "../../../api/file-api";
import { add } from "../../../api/vehicle-api";
import { showSuccess, showWarn } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { vehicleMessages } from "../../../shared/messages";
import { useRef, useState } from "react";
import { useSelector } from "react-redux";
export const AddCar = () => {
    const user = useSelector((state) => state.user.user);
    const childRef = useRef();
    const [loading, setLoading] = useState(false);
    const handleAddCar = async ({file, car}) =>{
        try {
            if (!file){
                showWarn(toastRef, "Please upload vehicle image.");
                return
            }
            setLoading(true);
            const imageUrl = await upload(file);
            await add({...car, host_id: user?.id, imageUrl});
            showSuccess(toastRef, vehicleMessages['vehicle-added-successfully']);
            childRef.current.handleFormReset();
            childRef.current.setImageView();
        } catch (e) {
            console.log(e);
        } finally {
          setLoading(false);
        }
    }

    return (
        <div className={styles.main}>
            <PageHeader header="Add car"/>
            <CarForm onSave={handleAddCar} ref={childRef} loader={loading}/>
        </div>
    )
}