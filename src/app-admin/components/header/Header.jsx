import styles from './Header.module.css'
import {InputText} from "primereact/inputtext";
import {Avatar} from "primereact/avatar";
import { TieredMenu } from "primereact/tieredmenu";
import { useRef } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Button } from "primereact/button";

export const Header = () => {
    const menu = useRef(null);
    const user = useSelector((state) => state.user.user);
    const router = useNavigate();

    const items = [
        {
            label: "Profile",
            icon: "pi pi-user",
            path: "/profile",
            command() {
                router(this.path);
            },
        },
        {
            label: "Admin",
            icon: "pi pi-user",
            path: "/admin",
            command() {
                router(this.path);
            },
        }
    ];

    return (
        <header className={`${styles.header} container`}>
            {/*<div className="p-inputgroup w-1/3">*/}
            {/*    <InputText placeholder="Search..."/>*/}
            {/*    <Button icon="pi pi-search" className="p-button-info"/>*/}
            {/*</div>*/}
            <nav className={styles.navigation}>
                <ul>
                    <div>
                        <Avatar
                            icon="pi pi-user"
                            image={user?.profileImg}
                            className="mr-2 border-2 border-gray-400" size="normal" shape="circle"
                            onClick={(e) => menu.current.toggle(e)}
                        />
                        <TieredMenu model={items} popup ref={menu} breakpoint="767px"/>
                    </div>
                </ul>
            </nav>
        </header>
    )
}