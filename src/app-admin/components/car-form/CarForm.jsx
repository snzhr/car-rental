import { InputText } from "primereact/inputtext";
import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import styles from "./CarForm.module.css";
import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";
import { carType, colors, fuel, transmission } from "../../../shared/static";
import { Calendar } from "primereact/calendar";
import { InputNumber } from "primereact/inputnumber";
import { read } from "../../../api/file-api";

export const CarForm = forwardRef(({vehicle, onSave, loader, user}, ref) => {
    const [imageView, setImageView] = useState(null);
    const [file, setFile] = useState(null);
    const [car, setCar] = useState({
        make: '',
        model: '',
        year: '',
        color: '',
        type: '',
        seats: '',
        transmission: '',
        fuelType: '',
        mileage: '',
        pricePerDay: 0,
        available: true,
        imageUrl: '',
        mpg: 0
    });

    useEffect(() => {
        if (vehicle) {
            setCar({
                ...vehicle,
                year: new Date(String(parseInt(vehicle.year) + 1)),
            });
            setImageView(vehicle.imageUrl);
        }
    }, [vehicle]);

    function handleInputChange(e) {
        const {name, value} = e.target;
        if (e.target.type === 'checkbox') {
            setCar({
                ...car, [name]: e.target.checked,
            });
            return;
        }
        setCar({
            ...car, [name]: value,
        });
    }

    function handleFormReset() {
        setCar({
            make: '',
            model: '',
            year: '',
            color: '',
            type: '',
            seats: '',
            transmission: '',
            fuelType: '',
            mileage: '',
            pricePerDay: 0,
            available: true,
            imageUrl: '',
            mpg: 0
        })
    }

    useImperativeHandle(ref, () => ({
        handleFormReset,
        setImageView
    }));

    async function handleFormSubmit(e) {
        e.preventDefault();
        onSave({file, car});
    }

    const onReadImage = async (e) => {
        e.stopPropagation();
        e.preventDefault();
        const file = e.target['files'][0];
        setFile(file);
        if (file) {
            await read(file, async (readImg) => {
                setImageView(readImg);
            })
        }
    }

    return (
        <form className="py-12 flex flex-col gap-8" onSubmit={handleFormSubmit}>
            <div className={styles.imageUrl} style={{backgroundImage: `url(${imageView})`}}>
                <label htmlFor="upload" className={styles.label}></label>
                <input type="file" id="upload" hidden onChange={onReadImage}/>
            </div>

            <div className="flex gap-3">
                <div className="card w-full">
                <span className="flex-auto">
                <label htmlFor="make" className="font-bold block mb-1">Make</label>
                <InputText
                    className={`${styles.password} w-full`}
                    inputId="make"
                    name="make"
                    value={car.make} onChange={handleInputChange}/>
                </span>
                </div>
                <div className="card w-full">
            <span className="flex-auto">
                <label htmlFor="model" className="font-bold block mb-1">Model</label>
                <InputText
                    className={`${styles.password} w-full`}
                    inputId="model"
                    name="model"
                    value={car.model} onChange={handleInputChange}/>
                </span>
                </div>
            </div>
            <div className="flex gap-3">
                <div className="card w-full">
                <span className="flex-auto">
                <label htmlFor="year" className="font-bold block mb-1">Year</label>
                    <Calendar className="w-full"
                              inputId="year"
                              value={car.year}
                              onChange={handleInputChange}
                              view="year"
                              name="year"
                              maxDate={new Date()}
                              dateFormat="yy"/>
                </span>
                </div>
                <div className="card w-full">
            <span className="flex-auto">
                <label htmlFor="color" className="font-bold block mb-1">Color</label>
                      <Dropdown
                          inputId="color"
                          name="color"
                          value={car.color}
                          onChange={handleInputChange}
                          options={colors}
                          optionLabel="name"
                          className="w-full"/>
                </span>
                </div>
            </div>
            <div className="flex gap-3">
                <div className="card w-full">
                <span className="flex-auto">
                <label htmlFor="type" className="font-bold block mb-1">Car type</label>
                      <Dropdown
                          inputId="type"
                          name="type"
                          value={car.type}
                          onChange={handleInputChange}
                          options={carType}
                          optionLabel="name"
                          className="w-full"/>
                </span>
                </div>
                <div className="card w-full">
            <span className="flex-auto">
                <label htmlFor="seats" className="font-bold block mb-1">Car seats</label>
                <InputNumber className="w-full"
                             inputId="seats"
                             name="seats"
                             value={car.seats}
                             onValueChange={handleInputChange}
                             mode="decimal"
                             showButtons
                             min={2}
                             max={7}/>
                </span>
                </div>
            </div>
            <div className="flex gap-3">
                <div className="card w-full">
                <span className="flex-auto">
                <label htmlFor="transmission" className="font-bold block mb-1">Transmission</label>
                    <Dropdown
                        inputId="type"
                        name="transmission"
                        value={car.transmission}
                        onChange={handleInputChange}
                        options={transmission}
                        optionLabel="name"
                        className="w-full"/>
                </span>
                </div>
                <div className="card w-full">
                    <span className="flex-auto">
                        <label htmlFor="fuelType" className="font-bold block mb-1">Fuel type</label>
                        <Dropdown
                            inputId="fuelType"
                            name="fuelType"
                            value={car.fuelType}
                            onChange={handleInputChange}
                            options={fuel}
                            optionLabel="name"
                            className="w-full"/>
                    </span>
                </div>
            </div>
            <div className="flex gap-3">
                <div className="card w-full">
                <span className="flex-auto">
                    <label htmlFor="mileage" className="font-bold block mb-1">Mile</label>
                <InputNumber
                    className="w-full"
                    inputId="mileage"
                    name="mileage"
                    value={car.mileage}
                    onValueChange={handleInputChange}
                    suffix=" mi"/>
                </span>
                </div>
                <div className="card w-full">
                    <span className="flex-auto">
                        <label htmlFor="pricePerDay" className="font-bold block mb-1">Price per day</label>
                        <InputNumber
                            className="w-full"
                            inputId="pricePerDay"
                            name="pricePerDay"
                            value={car.pricePerDay}
                            onValueChange={handleInputChange}
                            mode="currency"
                            disabled={user && user?.isAdmin === false}
                            tooltip="Only admin can modify this field"
                            tooltipOptions={{position: 'top', disabled: !user}}
                            currency="USD"
                            locale="en-US"/>
                    </span>
                </div>
            </div>

            <div className="flex gap-3">
                <div className="card w-full">
                <span className="flex-auto">
                    <label htmlFor="mpg" className="font-bold block mb-1">Miles per gallon (MPG)</label>
                <InputNumber
                    className="w-full"
                    inputId="mpg"
                    name="mpg"
                    value={car.mpg}
                    onValueChange={handleInputChange}
                    suffix=" mi"/>
                </span>
                </div>
                <div className="card w-full">
                <span className="flex-auto">
                    <label htmlFor="hostId" className="font-bold block mb-1">Host ID</label>
                <InputNumber
                    className="w-full"
                    inputId="hostId"
                    name="hostId"
                    value={car.host_id}
                    disabled
                    onValueChange={handleInputChange}/>
                </span>
                </div>
                {/*<div className="card w-full">*/}
                {/*    <span className="flex-auto">*/}
                {/*         <Checkbox inputId="available" name="available" checked={car.available}*/}
                {/*                   onChange={handleInputChange}/>*/}
                {/*            <label htmlFor="available" className="ml-2">Available</label>*/}
                {/*    </span>*/}
                {/*</div>*/}
            </div>
            <Button
                type="submit"
                className="w-1/4 self-end"
                label="Save"
                icon="pi pi-check"
                iconPos="right"
                loading={loader}
            />
        </form>
    )
});