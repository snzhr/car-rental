import styles from './StatisticsBlock.module.css';

export const StatisticsBlock = ({title, count, icon}) => {
    return (
        <div className={`${styles.main} border-gray-400 border`}>
            <div className="font-bold text-gray-600">
                <h1>{title}</h1>
                <h1 className="text-4xl">{count}</h1>
            </div>
            <div>
                <span className={`pi pi-${icon} text-3xl`}></span>
            </div>
        </div>
    )
}