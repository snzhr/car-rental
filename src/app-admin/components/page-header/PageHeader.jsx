import styles from './PageHeader.module.css';
import { Button } from "primereact/button";

export const PageHeader = ({hasBtn, btnClick, header}) => {

    return (
        <div className={styles.header}>
                <h1>{header}</h1>
            {
                hasBtn && <Button onClick={btnClick} label="Add vehicle" icon="pi pi-plus" />
            }
        </div>
    )
}