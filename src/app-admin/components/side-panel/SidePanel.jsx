import styles from './SidePanel.module.css'
import { useState } from "react";
import { NavLink } from "react-router-dom";
import { signout } from "../../../api/auth-api";
import { Button } from "primereact/button";
import { add, setToken } from "../../../store/slices/userSlice";
import { useDispatch } from "react-redux";
import router from "../../../routes/routes";

export const SidePanel = () => {
    const [panelState, setPanelState] = useState(true);
    const dispatch = useDispatch();
    const menu = router.routes[0].children[1].children;
    const handleLogout = async () => {
        await signout();
        dispatch(setToken(null));
    }

    const setActiveLink = ({isActive}) => isActive && styles['active-link'];

    return (
        <div className={`${styles['side-panel']} ${panelState ? styles.active : ''}`}>
            <div className={styles.logo}>
                <NavLink to='/'>UA</NavLink>
                {
                    panelState ?
                        <span onClick={() => setPanelState(false)} className="pi pi-chevron-left"></span>
                        :
                        <span onClick={() => setPanelState(true)} className="pi pi-chevron-right"></span>
                }
            </div>
            <div className={styles["side-content"]}>
                <nav className={styles.navigation}>

                    {
                        menu.map(item => {
                          if (item.isMain){
                              return (
                                  <Button key={item.id} unstyled tooltip={item.label} tooltipOptions={{disabled: panelState}}>
                                      <NavLink to={item.path}
                                               relative="route"
                                               className={setActiveLink}>
                                          <span className={item.icon}></span>
                                          <span className={!panelState ? styles['hidden'] : undefined}>{item.label}</span>
                                      </NavLink>
                                  </Button>
                              )
                          }
                        })
                    }
                    {/*<Button unstyled tooltip="Vehicles" tooltipOptions={{disabled: panelState}}>*/}
                    {/*    <NavLink to='vehicles' relative="route"*/}
                    {/*             className={setActiveLink}>*/}
                    {/*        <span className={`pi pi-car`}></span>*/}
                    {/*        <span className={!panelState ? styles['hidden'] : undefined}>Vehicles</span>*/}
                    {/*    </NavLink>*/}
                    {/*</Button>*/}
                    {/*<Button unstyled tooltip="Customers" tooltipOptions={{disabled: panelState}}>*/}
                    {/*    <NavLink to='customers' relative="route"*/}
                    {/*             className={setActiveLink}>*/}
                    {/*        <span className={`pi pi-user`}></span>*/}
                    {/*        <span className={!panelState ? styles['hidden'] : undefined}>Customers</span>*/}
                    {/*    </NavLink>*/}
                    {/*</Button>*/}
                    {/*<Button unstyled tooltip="Reservations" tooltipOptions={{disabled: panelState}}>*/}
                    {/*    <NavLink to='reservations' relative="route"*/}
                    {/*             className={setActiveLink}>*/}
                    {/*        <span className={`pi pi-file`}></span>*/}
                    {/*        <span className={!panelState ? styles['hidden'] : undefined}>Reservations</span>*/}
                    {/*    </NavLink>*/}
                    {/*</Button>*/}
                    {/*<Button unstyled tooltip="Settings" tooltipOptions={{disabled: panelState}}>*/}
                    {/*    <NavLink*/}
                    {/*        to='settings'*/}
                    {/*        relative="route"*/}
                    {/*        className={setActiveLink}>*/}
                    {/*        <span className={`pi pi-cog`}></span>*/}
                    {/*        <span className={!panelState ? styles['hidden'] : undefined}>Settings</span>*/}
                    {/*    </NavLink>*/}
                    {/*</Button>*/}
                </nav>
                <div className={styles['logout']}>
                    <Button tooltip="Logout" onClick={handleLogout} tooltipOptions={{disabled: panelState}}>
                        <span className={`pi pi-sign-out`}></span>
                        <span className={!panelState ? styles['hidden'] : undefined}>Logout</span>
                    </Button>
                </div>
            </div>
        </div>
    )
}