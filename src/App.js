import './App.css';
import { Outlet } from "react-router-dom";
import { PrimeReactProvider } from "primereact/api";
import { useEffect, useRef, useState } from "react";
import { add, setToken } from "./store/slices/userSlice";
import { useDispatch, useSelector } from "react-redux";
import { Toast } from "primereact/toast";
import { Loader } from "./shared/ui/Loader/Loader";
import { getMe } from "./api/auth-api";

export let toastRef = null;

function App() {
    toastRef = useRef(null);
    const dispatch = useDispatch();
    const [userId, setUserId] = useState('');
    const {loaderState} = useSelector(state => state.loader);
    const { token } = useSelector(state => state.user);

    // useEffect(() => {
    //     const unsubscribe = onAuthStateChanged(auth, async (user) => {
    //         if (user) {
    //             //TODO: fix bug when user registers
    //             setUserId(user.uid);
    //         } else {
    //             setUserId(null);
    //             dispatch(add(null));
    //         }
    //     });
    //
    //     return () => unsubscribe();
    // }, []);

    // useEffect(() => {
    //     if (userId) {
    //         async function getCurrentUser() {
    //             try {
    //                 dispatch(setLoaderState(true))
    //                 const res = await getUser(userId);
    //                 dispatch(add(res.data));
    //             } catch (e) {
    //                 console.log(e);
    //             }finally {
    //                 dispatch(setLoaderState(false))
    //             }
    //         }
    //
    //         getCurrentUser();
    //     }
    // }, [userId]);

    async function getUser(){
        try {
            const res = await getMe();
            dispatch(add(res.data));
        }catch (e) {
            console.log(e);
        }
    }


    useEffect(() => {
        const authToken = localStorage.getItem('token');
        if (authToken === "null"){
            dispatch(setToken(null));
        }else{
            dispatch(setToken(authToken));
        }
        if (token){
            getUser();
        }else{
            dispatch(add(null));
        }
    }, [token]);

    return (
        <PrimeReactProvider>
            {loaderState && <Loader/>}
            <Toast ref={toastRef}/>
            <Outlet/>
        </PrimeReactProvider>
    );
}

export default App;
