import styles from "./Header.module.css";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { signout } from "../../../api/auth-api";
import { Avatar } from "primereact/avatar";
import { TieredMenu } from "primereact/tieredmenu";
import { useEffect, useRef, useState } from "react";
import logo from "../../../assets/images/main-logo.png";
import { setToken } from "../../../store/slices/userSlice";


export const Header = () => {
    const menu = useRef(null);
    const user = useSelector((state) => state.user.user);
    const router = useNavigate();
    const dispatch = useDispatch();
    const [menuItems, setMenuItems] = useState([
        {
            label: "Profile",
            icon: "pi pi-user",
            path: "profile",
            command() {
                router(this.path);
            },
        },
        {
            separator: true,
        },
        {
            label: "Logout",
            icon: "pi pi-sign-out",
            command: () => handleLogout(),
        },
    ]);


    useEffect(() => {
        if (user && user.isAdmin){
            const adminMenuItem = {
                label: "Admin",
                icon: "pi pi-user",
                path: "admin",
                command() {
                    router(this.path);
                },
            }
            const _menuItems = [...menuItems];
            _menuItems[1] = adminMenuItem;
            setMenuItems(_menuItems);
        }

    }, [user]);

    const handleLogout = async () => {
        await signout();
        dispatch(setToken(null));

    };

    return (
        <header className={`${styles.header}`}>
            <nav className={styles.nav}>
                    <NavLink to="/">
                        <img src={logo} style={{width: '100px'}} alt="logo" />
                    </NavLink>
                <div className={styles.links}>
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/all-vehicles">All vehicles</NavLink>
                    <NavLink to="#">About us</NavLink>
                    <NavLink to="#">Info</NavLink>
                    <NavLink to="#">Contact us</NavLink>

                    {!user ? (
                        <NavLink to="/login">Login</NavLink>
                    ) : (
                        <div>
                            <Avatar
                                icon="pi pi-user"
                                image={user.profileImg}
                                className="mr-2 border-2 border-gray-400" size="normal" shape="circle"
                                onClick={(e) => menu.current.toggle(e)}
                            />
                            <TieredMenu model={menuItems} popup ref={menu} breakpoint="767px"/>
                        </div>
                    )}
                </div>
            </nav>
        </header>
    );
};
