import { InputTextarea } from "primereact/inputtextarea";
import { useEffect, useState } from "react";
import { Button } from "primereact/button";
import { Rating } from "primereact/rating";
import { Divider } from "primereact/divider";
import { add, getAll, remove } from "../../../api/review-api";
import { useSelector } from "react-redux";
import { SingleReview } from "../single-review/SingleReview";

export const Reviews = ({vehicleId}) => {
    const user = useSelector((state) => state.user.user);
    const [review, setReview] = useState({comment: ''});
    const [loader, setLoader] = useState(false);
    const [reviews, setReviews] = useState([]);
    const [commentLoader, setCommentLoader] = useState(false);

    async function getReviews() {
        try {
            const res = await getAll({vehicle_id: vehicleId});
            setReviews(res.data);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getReviews();
    }, []);

    async function handleFormSubmit(e) {
        e.preventDefault();
        try {
            setLoader(true);
            const data = {
                ...review,
                review_date: new Date(),
                vehicles_id: vehicleId,
                users_id: user?.id
            };
            await add(data);
        } catch (e) {
            console.log(e);
        } finally {
            const resetted = {rating: 0, comment: '', review_date: null};
            setReview(resetted);
            setLoader(false);
            getReviews();
        }
    }

    function handleInputChange(e) {
        const {name, value} = e.target;
        setReview({
            ...review,
            [name]: value,
        })
    }

    async function handleReviewDelete(id) {
        try {
            setCommentLoader(true);
            await remove(id);
            getReviews();
        } catch (e) {
            console.log(e);
        } finally {
            setCommentLoader(false);
        }
    }


    return (
        <div className="reviews-block w-full flex flex-col gap-8 px-10 py-5">
            <Divider/>
            <form onSubmit={handleFormSubmit} className="w-1/2 mx-auto items-center review-form flex flex-col gap-4">
                <div className="card">
                    <Rating
                        name="rating"
                        value={review?.rating}
                        onChange={handleInputChange}
                        cancel={false}
                    />
                </div>
                <div className="card flex flex-col justify-content-center">
                    <label htmlFor="comment">Comment</label>
                    <InputTextarea
                        name="comment"
                        className="w-full"
                        id="comment"
                        value={review?.comment}
                        onChange={handleInputChange}
                        rows={5}
                        cols={100}
                    />
                </div>
                {
                    !user &&
                    <span className="text-center text-red-600">
                        In order to rate this vehicle, you have to log in to the system
                    </span>
                }
                    <Button
                        loading={loader}
                        className="self-center"
                        type="submit"
                        label="Submit"
                        outlined
                        disabled={ review?.comment === '' || !review?.rating || !user}
                    />
            </form>
            <Divider/>
            <div className="w-2/3 mx-auto reviews flex flex-col gap-4 items-center">
                <h3 className="font-bold uppercase text-xl">Reviews</h3>
                {
                    !reviews?.length ? <h1 className="text-xl">No reviews so far</h1> :
                    reviews?.map(review => {
                        return <SingleReview loader={commentLoader} onDelete={handleReviewDelete}
                                             key={review.id} {...review}/>
                    })
                }
            </div>
        </div>
    )
}