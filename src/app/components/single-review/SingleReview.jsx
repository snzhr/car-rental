import { Rating } from "primereact/rating";
import { useSelector } from "react-redux";
import { Button } from "primereact/button";


export const SingleReview = ({comment, id, author, rating, onDelete, loader}) => {
    const user = useSelector((state) => state.user.user);

    return (
        <div className="flex gap-4 bg-gray-300 w-3/4 justify-between p-4 rounded-2xl">
            <div className="flex gap-4">
            <div className="w-10 h-10 border border-gray-900 rounded-3xl bg-contain bg-no-repeat bg-center self-center"
                 style={{backgroundImage: `url(${author.profileImg})`}}>
            </div>
            <div className="flex flex-col gap-2">
                <Rating value={rating} readOnly cancel={false}/>
                <p className="font-bold">{author.name} {author.lastname}</p>
                <p className="italic">{comment}</p>
            </div>
            </div>
            {author.id === user?.id &&
                <Button
                    loading={loader}
                    rounded
                    outlined
                    onClick={() => onDelete(id)}
                    tooltip="Delete this comment"
                    icon="pi pi-trash"
                    className="text-red-700 self-center ml-8" />
            }
        </div>
    )
}