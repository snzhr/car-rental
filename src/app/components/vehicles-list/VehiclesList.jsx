import styles from './VehiclesList.module.css';
import { CarCard } from "../../../shared/ui/car-card/CarCard";
import { useNavigate } from "react-router-dom";

export const VehiclesList = ({vehicles}) => {
    const navigate = useNavigate();

    const handleVehicle = (id) => {
        navigate(`/all-vehicles/${id}`);
    }

    return (
        <div className={styles.list}>
            {
                vehicles?.map(vehicle => {
                    return <CarCard
                        key={vehicle.id}
                        car={vehicle}
                        handleRentClick={handleVehicle}/>
                })
            }
        </div>
    )
}