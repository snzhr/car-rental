import { CarForm } from "../../../app-admin/components/car-form/CarForm";
import { PageHeader } from "../../../app-admin/components/page-header/PageHeader";
import { useSelector } from "react-redux";
import { useRef, useState } from "react";
import { add } from "../../../api/vehicle-api";
import { upload } from "../../../api/file-api";
import { showSuccess, showWarn } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { vehicleMessages } from "../../../shared/messages";

export const AddHostCar = () => {
    const {user} = useSelector(state => state.user);
    const childRef = useRef();
    const [loading, setLoading] = useState(false);

    const handleAddCar = async ({file, car}) => {
        try {
            if (!file) {
                showWarn(toastRef, "Please upload vehicle image.");
                return
            }
            setLoading(true);
            const imageUrl = await upload(file);
            await add({...car, host_id: user?.id, imageUrl});
            showSuccess(toastRef, vehicleMessages['vehicle-added-successfully']);
            childRef.current.handleFormReset();
            childRef.current.setImageView();
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }
    return (
        <div className="p-10">
            <PageHeader header="Host: add vehicle"/>
            <CarForm onSave={handleAddCar} loader={loading} user={user} ref={childRef}/>
        </div>
    )
}