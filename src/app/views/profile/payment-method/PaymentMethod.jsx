import { InputText } from "primereact/inputtext";
import { useState } from "react";
import { usaStates } from "../../../../shared/static";
import { Dropdown } from "primereact/dropdown";

export const PaymentMethod = () => {
    const [value, setValue] = useState('');

    const [selectedCountry, setSelectedCountry] = useState("United States");
    const [selectedState, setSelectedState] = useState();

    const countries = [
         {
            name: 'United States',
            code: 'US',
        }
    ];


    return (
        <form className="py-12 flex flex-col gap-8">
            <div className="card">
                <span className="p-float-label w-full">
                 <InputText type="text" className="w-full" id="card-number" value={value}
                            onChange={(e) => setValue(e.target.value)}/>
                <label htmlFor="card-number">Card number</label>
                </span>
            </div>

            <div className="card">
                <span className="p-float-label w-full">
                 <InputText type="text" className="w-full" id="address" value={value}
                            onChange={(e) => setValue(e.target.value)}/>
                <label htmlFor="address">Street address</label>
                </span>
            </div>

            <div className="card">
                <span className="p-float-label w-full">
                 <InputText type="text" className="w-full" id="address-2" value={value}
                            onChange={(e) => setValue(e.target.value)}/>
                <label htmlFor="address-2">Apt, unit, suite, etc. (optional)</label>
                </span>
            </div>

            <div className="card flex justify-content-center">
                <Dropdown value={selectedCountry} onChange={(e) => setSelectedCountry(e.value)} options={countries}
                          optionLabel="name"
                          placeholder="Select country" className="w-full md:w-14rem"/>
            </div>

            <div className="card flex gap-6">
                <span className="p-float-label w-full">
                 <InputText type="text" className="w-full" id="city" value={value}
                            onChange={(e) => setValue(e.target.value)}/>
                <label htmlFor="city">City</label>
                </span>

                <div className="card flex justify-content-center">
                    <Dropdown value={selectedState} onChange={(e) => setSelectedState(e.value)} options={usaStates}
                              optionLabel="name"
                              placeholder="Select state" className="w-full md:w-14rem"/>
                </div>

                <span className="p-float-label w-full">
                 <InputText type="text" className="w-full" id="zipcode" value={value}
                            onChange={(e) => setValue(e.target.value)}/>
                <label htmlFor="zipcode">Zip code</label>
                </span>
            </div>
        </form>
    )
}