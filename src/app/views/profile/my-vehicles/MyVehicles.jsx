import { useEffect, useState } from "react";
import { getAll } from "../../../../api/vehicle-api";
import { getAllRequests } from "../../../../api/host-request-api";
import { useSelector } from "react-redux";
import { Button } from "primereact/button";
import { useNavigate } from "react-router-dom";
import { CarCard } from "../../../../shared/ui/car-card/CarCard";
import { ProgressSpinner } from "primereact/progressspinner";
import { confirmDialog, ConfirmDialog } from "primereact/confirmdialog";
import { showSuccess } from "../../../../shared/notification";
import { toastRef } from "../../../../App";
import { update } from "../../../../api/host-request-api";

export const MyVehicles = () => {
    const {user} = useSelector((state) => state.user);
    const [vehicles, setVehicles] = useState([]);
    const router = useNavigate();
    const [loading, setLoading] = useState(false);


    const getVehicles = async () => {
        try {
            setLoading(true);
            const res = await getAll({hostId: user?.id});
            setVehicles(res.data.items);
        } catch (error) {
            console.log(error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        if (user) {
            getVehicles();
        }
    }, [user]);

    const handleEditVehicle = (id) => {
        router(`/host/update-vehicle/${id}`);
    }

    const handleAddVehicle = () => {
        router('/host/add-vehicle');
    }

    async function removeCar(data) {
        try {
            const res = await getAllRequests({userId: user?.id});
            const body = {
                ...res.data[0], removal_requests: {
                    vehicle_id: data.id
                }
            }
            await update(res.data[0].id, body);
            showSuccess(toastRef, "Request was successfully sent.");
        } catch (e) {
            console.log(e);
        }
    }

    const confirm2 = (data) => {
        confirmDialog({
            message: 'Do you want to send vehicle removal request?',
            header: 'Action Confirmation',
            icon: 'pi pi-check-circle',
            defaultFocus: 'accept',
            acceptClassName: 'p-button-danger',
            accept: async () => {
                await removeCar(data);
            },
        });
    };

    const noVehiclesTemplate = <div className="h-80 flex gap-4 flex-col justify-center items-center">
        <h1 className="text-2xl">You do not have any vehicles yet.</h1>
        <Button label="Add vehicle" icon="pi pi-plus" iconPos="right" onClick={handleAddVehicle}/>
    </div>


    return (
        <div className="py-3">
            {
                loading ?
                    <div>
                        <ProgressSpinner/>
                    </div>
                    :
                    vehicles.length === 0 ? noVehiclesTemplate :
                        <div className="flex flex-col gap-5">
                            <div className="flex flex-wrap gap-4">
                                {vehicles.map(vehicle => {
                                    return <CarCard
                                        key={vehicle.id}
                                        car={vehicle}
                                        handleEdit={handleEditVehicle}
                                        handleRentClick={() => null}
                                        handleDeleteClick={() => confirm2(vehicle)}
                                    />
                                })
                                }
                            </div>
                            <Button
                                icon="pi pi-plus"
                                iconPos="right"
                                label="Add another vehicle"
                                className="self-center"
                                onClick={handleAddVehicle}
                            />
                        </div>
            }
            <ConfirmDialog/>
        </div>
    )
}