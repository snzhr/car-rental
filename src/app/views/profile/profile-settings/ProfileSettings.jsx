import { InputText } from "primereact/inputtext";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Button } from "primereact/button";
import { updateUser } from "../../../../api/user-api";
import { toastRef } from "../../../../App";
import { showError, showSuccess } from "../../../../shared/notification";
import { messages, userMessages } from "../../../../shared/messages";

export const ProfileSettings = () => {
    const user = useSelector(state => state.user.user);
    const [loading, setLoading] = useState(false);
    const [localUser, setLocalUser] =
        useState({
            name: '',
            lastname: '',
            phone: ''
        });

    useEffect(() => {
        setLoading(true);
        setLocalUser({...user});
        setLoading(false);
    }, [user]);
    function handleInputChange(e) {
        const {name, value} = e.target;
        setLocalUser({
            ...localUser,
            [name]: value,
        });
    }

    async function handleFormSubmit(e) {
        e.preventDefault();
        try {
          await updateUser(user.id, localUser);
            showSuccess(toastRef, userMessages["user-updated"]);
        }catch (e) {
            showError(toastRef, messages[e.code]);
        }
    }

    return (
            <>
                { loading === true ? (<p>Loading</p>) :
            (<form className="py-12 flex flex-col gap-8" onSubmit={(e) => handleFormSubmit(e)}>
                    <div className="card flex gap-6 justify-content-center">
               <span className="p-float-label w-full">
                 <InputText className="w-full" id="name" name="name" value={localUser?.name || ''}
                            onChange={handleInputChange}/>
                <label htmlFor="name">Name</label>
                </span>

                        <span className="p-float-label w-full">
                 <InputText className="w-full" id="lastname" name="lastname" value={localUser?.lastname || ''}
                            onChange={handleInputChange}/>
                <label htmlFor="lastname">Surname</label>
                </span>
                    </div>

                    <div className="card">
                <span className="p-float-label w-full">
                 <InputText disabled type="email" className="w-full" id="email" name="email" value={localUser?.email || ''}
                            onChange={handleInputChange}/>
                <label htmlFor="email">Email</label>
                </span>
                    </div>

                    <div className="card">
                <span className="p-float-label w-full">
                 <InputText type="tel" className="w-full" id="phone" name="phone" value={localUser?.phone || ''}
                            onChange={handleInputChange}/>
                <label htmlFor="phone">Phone number</label>
                </span>
                    </div>
                    <Button type="submit" className="w-1/4 self-end" label="Save" icon="pi pi-check" iconPos="right" />
                </form>
            )
                }
            </>
    )
}