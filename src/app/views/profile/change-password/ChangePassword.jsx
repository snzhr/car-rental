import styles from "./ChangePassword.module.css";
import { useState } from "react";
import { Password } from "primereact/password";
import { Button } from "primereact/button";
import { resetPassword } from "../../../../api/user-api";
import { showError, showSuccess, showWarn } from "../../../../shared/notification";
import { toastRef } from "../../../../App";
import { formMessages, messages } from "../../../../shared/messages";
import { useSelector } from "react-redux";

export const ChangePassword = () => {
    const user = useSelector(state => state.user.user);
    const [passwords, setPasswords] =
        useState({
            oldPassword: '',
            password: '',
            confirmation: ''
        });

    function handleInputChange(e) {
        const {name, value} = e.target;
        setPasswords({
            ...passwords,
            [name]: value,
        });
    }

    async function handleFormSubmit(e) {
        e.preventDefault();
        if (!passwords.oldPassword){
            showWarn(toastRef, formMessages["provide-old-password"]);
            return
        }

        if (passwords.password !== passwords.confirmation){
            showWarn(toastRef, formMessages["password-not-match"]);
            return
        }
        try {
            const credentials = {email: user.email, password: passwords.oldPassword}
            await resetPassword(passwords.password, credentials);
            showSuccess(toastRef, formMessages["password-success"]);
        }catch (e) {
            showError(toastRef, messages[e.code]);
        }
    }

    return (
        <>
            <form className="py-12 flex flex-col gap-8" onSubmit={handleFormSubmit}>
                <div className="card w-full">
            <span className="p-float-label">
                <Password className={`${styles.password} w-full`} inputId="oldpassword" name="oldPassword"
                          value={passwords.oldPassword} onChange={handleInputChange}/>
                <label htmlFor="oldpassword">Old password</label>
            </span>
                </div>
                <div className="flex gap-3">
                    <div className="card w-full">
            <span className="p-float-label">
                <Password className={`${styles.password} w-full`} inputId="password1" name="password"
                          value={passwords.password} onChange={handleInputChange}/>
                <label htmlFor="password1">New password</label>
            </span>
                    </div>
                    <div className="card w-full">
            <span className="p-float-label">
                <Password className={`${styles.password} w-full`} inputId="password2" name="confirmation"
                          value={passwords.confirmation} onChange={handleInputChange}/>
                <label htmlFor="password2">Confirm new password</label>
            </span>
                    </div>
                </div>
                <Button type="submit" className="w-1/4 self-end" label="Save" icon="pi pi-check" iconPos="right"/>
            </form>
        </>
    )
}