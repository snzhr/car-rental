import { useEffect, useRef, useState } from "react";
import { getAll, remove, update } from "../../../../api/rent-api";
import { useSelector } from "react-redux";
import { ProgressSpinner } from "primereact/progressspinner";
import { Button } from "primereact/button";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { RentalStatusTag } from "../../../../shared/ui/rental-status-tag/RentalStatusTag";

export const MyRentals = () => {
    const btnRef = useRef(null);
    const removeBtnRef = useRef(null);
    const user = useSelector((state) => state.user.user);
    const [rentals, setRentals] = useState([]);
    const [loading, setLoading] = useState(false);

    async function getRentals() {
        try {
            setLoading(true);
            const res = await getAll({userId: user?.id});
            setRentals(res.data.items);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        if (user){
            getRentals();
        }
    }, [user]);

    async function handleCancelClick(rental) {
        try {
            setLoading(true);
            const cancelledRental = {
                ...rental,
                status: "cancelled",
                start_date: null,
                end_date: null,
            };

            await update(rental.id, cancelledRental);
            await getRentals();
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    async function handleDeleteClick(id) {
        try {
            setLoading(true);
            await remove(id);
            await getRentals();
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    const confirm = (rental, action, message) => {
        confirmDialog({
            message: message,
            header: 'Action Confirmation',
            icon: 'pi pi-info-circle',
            defaultFocus: 'reject',
            acceptClassName: 'p-button-danger',
            async accept() {
                if (action === 'cancel'){
                    await handleCancelClick(rental);
                }else{
                    await handleDeleteClick(rental.id);
                }
            }
        });
    };

    return (
        <div className="py-3.5 flex flex-col gap-6">
            {
                loading ?
                    <ProgressSpinner/>
                    :
                    (
                        !rentals.length
                            ?
                            <div className="h-96 flex items-center justify-center">
                                <p className="font-bold text-gray-600">You don't have any rented vehicles at the moment</p>
                            </div>
                            :
                    rentals?.map(rental => {
                        return (
                            <div
                                key={rental.id}
                                className="rounded-t-lg rounded-b-lg "
                                style={{boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'}}
                            >
                                <div
                                    className="text-white gap-8 h-10 bg-teal-600 rounded-t-lg flex uppercase justify-start items-center p-4">
                                    <p className="w-1/2 text-center">Vehicle</p>
                                    <p className="w-1/3 text-center">Status</p>
                                    <p className="w-1/2 text-center">Period</p>
                                    <p className="w-1/3 text-center">Total cost</p>
                                    <p className="w-1/3 text-center">Actions</p>
                                </div>
                                <div className="p-4 flex gap-8 items-center justify-center">
                                    <div className="w-1/2 flex flex-col items-center">
                                        <img
                                            src={rental.rental_vehicle.imageUrl}
                                            alt="Car image"
                                            className="w-3/4 block"/>
                                        <h2 className="">{rental.rental_vehicle.make} {rental.rental_vehicle.model}</h2>
                                    </div>
                                    <RentalStatusTag status={rental.status} />
                                    {
                                        (!rental.start_date && !rental.end_date) ?
                                            <p className="w-1/2 text-center">No data</p>
                                            :
                                            <p className="w-1/2 text-center">
                                                {rental.start_date}
                                                <br/> to <br/>
                                                {rental.end_date}
                                            </p>
                                    }
                                    <p className="w-1/3 text-center">${rental.total_cost}</p>
                                    <div className="w-1/3 text-center flex gap-2">
                                        <Button
                                            disabled={rental.status === "cancelled" || rental.status === "returned"}
                                            tooltip="Cancel reservation"
                                            tooltipOptions={{position: 'top'}}
                                            icon="pi pi-ban"
                                            rounded
                                            outlined
                                            severity="danger"
                                            aria-label="Cancel"
                                            onClick={() => confirm(rental, 'cancel', 'Do you want to cancel this reservation?')}
                                            ref={btnRef}
                                        />
                                        <Button
                                            disabled={rental.status !== "cancelled" && rental.status !== "returned"}
                                            tooltip="Delete reservation"
                                            tooltipOptions={{position: 'top'}}
                                            icon="pi pi-trash"
                                            rounded
                                            outlined
                                            severity="danger"
                                            aria-label="Cancel"
                                            onClick={() => confirm(rental, 'delete', 'Do you want to delete this reservation?')}
                                            ref={removeBtnRef}
                                        />
                                    </div>
                                </div>
                            </div>
                        )
                    })
                    )
            }
            <ConfirmDialog />
        </div>
    )
}