import styles from "./Profile.module.css";
import { useDispatch, useSelector } from "react-redux";
import { TabMenu } from "primereact/tabmenu";
import { useEffect, useRef, useState } from "react";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import avatarPlaceholder from "../../../assets/images/user_profile_fallback.jpg";
import { upload, read } from "../../../api/file-api";
import { updateUser } from "../../../api/user-api";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { Checkbox } from "primereact/checkbox";
import { add, getAllRequests } from "../../../api/host-request-api";
import { showInfo, showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";

const items = [
    {label: "Profile settings", icon: "pi pi-user-edit", path: "profile"},
    {label: "Payment methods", icon: "pi pi-wallet", path: "payment-method"},
    // {label: "Change password", icon: "pi pi-lock", path: "change-password"},
    {label: "My rentals", icon: "pi pi-receipt", path: "my-rentals"},
    {label: "My vehicles", icon: "pi pi-car", path: "my-vehicles", disabled: true},
];

export const Profile = () => {
    const user = useSelector((state) => state.user.user);
    const router = useNavigate();
    const tabMenu = useRef();
    let location = useLocation();
    const [activeTab, setActiveTab] = useState(null);
    const [activeIndex, setActiveIndex] = useState(0);
    const [avatar, setAvatar] = useState(null);
    const [file, setFile] = useState(null);
    const [visible, setVisible] = useState(false);
    const [checked, setChecked] = useState(false);
    const [requestLoader, setRequestLoader] = useState(false);
    const [hostRequest, setHostRequest] = useState(null);

    useEffect(() => {
        switch (activeTab) {
            case "profile":
                router("");
                setActiveIndex(0);
                break;
            case "payment-method":
                router("payment-method");
                setActiveIndex(1);
                break;
            // case "change-password":
            //     router("change-password");
            //     setActiveIndex(2);
            //     break;
            case "my-rentals":
                router("my-rentals");
                setActiveIndex(2);
                break;
            case "my-vehicles":
                router("my-vehicles");
                setActiveIndex(3);
                break;
            default:
                break;
        }
    }, [activeTab]);

    const getHostRequest = async () => {
        try {
            const res = await getAllRequests({userId: user?.id});
            setHostRequest(res.data[0]);
            if (res.data[0] && res.data[0]?.isApproved === 'approved'){
                items[3].disabled = false;
            }else{
                items[3].disabled = true;
            }
        }catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        const pathName = location.pathname.split('/');
        const path = pathName[pathName.length - 1];
        setActiveTab(path);
    }, [location]);

    useEffect(() => {
        if (user && user.profileImg) {
            setAvatar(user.profileImg);
        } else {
            setAvatar(avatarPlaceholder);
        }
        if (user){
            getHostRequest()
        }
    }, [user]);

    const onUpload = async () => {
        if (file) {
            const fileUrl = await upload(file);
            const localUser = {...user, profileImg: fileUrl};
            await updateUser(user.id, localUser);
        }
    };

    const onReadImage = async (e) => {
        e.preventDefault();
        e.stopPropagation();
        const file = e.target["files"][0];
        setFile(file);
        if (file) {
            await read(file, async (readImg) => {
                setAvatar(readImg);
            });
        }
    };

    const handleHostRequest = async () => {
        if (!checked) {
            showInfo(toastRef, "Please, make sure that you've read the contract and agree")
            return
        }
        setRequestLoader(true);
        try {
            const body = {
                users_id: user?.id,
                start_date: new Date(),
                isApproved: 'pending'
            }
            await add(body);
            showSuccess(toastRef, "Your request has been successfully sent. Our administration will take a look at your request and approve.", 7000)
            getHostRequest();
        } catch (e) {
            console.log(e);
        } finally {
            setRequestLoader(false);
            setVisible(false);
        }
    }

    return (
        <main className={styles.profile}>
            <section>
                <div
                    className={styles["profile-img"]}
                    style={{backgroundImage: `url(${avatar})`}}
                >
                    <input type="file" id="upload" hidden onChange={onReadImage}/>
                    <label htmlFor="upload" className={styles.label}></label>
                    <button onClick={onUpload}>
                        <span className="pi pi-save"></span>
                    </button>
                </div>
                <div className="text-center flex flex-col gap-4 px-10">
                    <h2>
                        {user?.name} {user?.lastname}
                    </h2>
                    {
                        (hostRequest && hostRequest?.isApproved === 'pending') &&
                    <h4 className="text-sm text-red-600">
                        You've already sent host request. We will approve it as soon as possible.
                        Thank you for your patience.
                    </h4>
                    }
                    <Button disabled={hostRequest} label="Become host" onClick={() => setVisible(true)}/>
                    </div>
            </section>
            <section>
                <h2>Profile information</h2>
                <div>
                    <TabMenu
                        ref={tabMenu}
                        activeIndex={activeIndex}
                        model={items}
                        onTabChange={(e) => {
                            setActiveTab(e.value["path"]);
                        }}
                    />
                    <div>
                        <Outlet/>
                    </div>
                </div>
            </section>
            <Dialog header="Become host" visible={visible} style={{width: '50vw'}} onHide={() => setVisible(false)}>
                <p className="m-0">
                    This agreement is made between <span
                    className="font-bold">"{user?.name} {user?.lastname}"</span> ("Host")
                    and <span className="font-bold">"Unaa-al LLC."</span> ("Company").
                    Host agrees to provide Company with the vehicle for rental purposes, and in return,
                    Company agrees to pay Host a percentage of the rental income generated.
                    Both parties agree to abide by the terms outlined herein.
                </p>
                <div className="card w-full mt-10 flex justify-between">
                    <span className="flex gap-2">
                            <label htmlFor="available" className="ml-2">I have read and agree</label>
                         <Checkbox inputId="available" name="available" checked={checked}
                                   onChange={(e) => setChecked(e.checked)}/>
                    </span>
                    <Button label="Send request" onClick={handleHostRequest} loading={requestLoader}/>
                </div>
            </Dialog>
        </main>
    );
};
