import { Button } from "primereact/button";
import { Calendar } from "primereact/calendar";
import imageCar from "../../../assets/images/hero_bg.jpg";
import "primeicons/primeicons.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { VehiclesList } from "../../components/vehicles-list/VehiclesList";
import { useDispatch } from "react-redux";
import { setLoaderState } from "../../../store/slices/loaderSlice";
import { getAll, getAvailable } from "../../../api/vehicle-api";
import { Dialog } from "primereact/dialog";
import { Pagination } from "../../../shared/ui/pagination/Pagination";
import { showInfo } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { CarForm } from "../../../app-admin/components/car-form/CarForm";

function Home() {
    let navigate = useNavigate();
    const dispatch = useDispatch();
    const [vehicles, setVehicles] = useState([]);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [visible, setVisible] = useState(false);
    const [availableVehicles, setAvailableVehicles] = useState([]);
    const [paging, setPaging] = useState({
        items: 0,
        page: 1,
        perPage: 5,
        startDate,
        endDate
    });
    const [query, setQuery] = useState({...paging});

    useEffect(() => {
        (async function () {
            dispatch(setLoaderState(true));
            try {
                const response = await getAll({page:1, perPage: 3});
                setVehicles(response.data.items);
            }catch (e) {
                console.log(e);
            }finally {
                dispatch(setLoaderState(false));
            }
        })()
    }, []);

    async function getAvailableCars(){
        const params = {
            startDate: startDate,
            endDate: endDate,
            ...query
        }
        try {
            const res = await getAvailable(params);
            setAvailableVehicles(res.data.items);
            setPaging({...paging, items: res.data.itemsTotal});
        }catch (e) {
            console.log(e);
        }
    }

    const handleBookNowClick = async () => {
        if (startDate === null || endDate === null) {
            showInfo(toastRef, "Please choose pick-up and drop-off dates");
            return;
        }
        setQuery({...query, startDate: startDate, endDate: endDate});
        setVisible(true);
    };

    const handleButtonClick = () => {
        navigate("/all-vehicles");
    }

    useEffect(() => {
        if (query.startDate && query.endDate){
            getAvailableCars();
        }
    }, [query]);

    const handlePageChange = (e) => {
        setQuery({...query, page: e.page + 1, perPage: e.rows});
    }

    return (
        <>
            <div className="hero-section text-white py-60 bg-cover"
                 style={{backgroundImage: `url(${imageCar})`}}>
                <div className="container mx-auto text-center">
                    <h1 className="text-4xl font-bold mb-4">Car Rental</h1>
                    <p className="text-lg mb-8">
                        Your Journey Starts Here. Explore Our Fleet of Quality Cars.
                    </p>
                    <div className="controls flex flex-col items-center justify-center gap-4">
                        <div className="dropdowns flex justify-center gap-6">
                            <Calendar
                                value={startDate}
                                onChange={(e) => setStartDate(e.value)}
                                dateFormat="mm/dd/yy"
                                placeholder="Pick-up Date"
                                showIcon
                                minDate={new Date()}
                                maxDate={endDate}
                                selectOtherMonths
                                icon="pi pi-calendar"
                            />
                            <Calendar
                                value={endDate}
                                onChange={(e) => setEndDate(e.value)}
                                dateFormat="mm/dd/yy"
                                placeholder="Return Date"
                                showIcon
                                minDate={startDate || new Date()}
                                selectOtherMonths
                                icon="pi pi-calendar"
                            />
                        </div>
                        <div className="buttons flex justify-center gap-4 mb-8">
                            <Button
                                label="Search"
                                className="book-button"
                                onClick={handleBookNowClick}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex flex-col gap-4 p-4">
                <h1 className="text-center text-2xl font-bold">Browse our latest vehicles</h1>
                <VehiclesList vehicles={vehicles}/>
                <Button onClick={handleButtonClick} className="self-center" iconPos="right" rounded label="Browse all" severity="secondary"
                        icon="pi pi-angle-right" outlined/>
            </div>
            <Dialog header="Available vehicles" className="bg-white" blockScroll visible={visible} maximized style={{width: '50vw'}}
                    onHide={() => setVisible(false)}>
                <div className="p-5">
                    {availableVehicles?.length ? <VehiclesList vehicles={availableVehicles}/> : <h3>No vehicles found</h3>}
                    <Pagination totalItems={paging.items} onPageClick={handlePageChange}/>
                </div>
            </Dialog>
        </>
    );
}

export default Home;
