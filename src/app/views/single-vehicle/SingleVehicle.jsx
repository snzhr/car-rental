import styles from './SingleVehicle.module.css';
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { setLoaderState } from "../../../store/slices/loaderSlice";
import { getOne } from "../../../api/vehicle-api";
import { getAll } from "../../../api/rent-api";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { Calendar } from "primereact/calendar";
import { add } from "../../../api/rent-api";
import { dateMaker, getRentedDates } from "../../../shared/utils/dateMaker";
import { confirmPopup, ConfirmPopup } from "primereact/confirmpopup";
import { showSuccess } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { Reviews } from "../../components/reviews/Reviews";

export const SingleVehicle = () => {
    const {id} = useParams();
    const user = useSelector((state) => state.user.user);
    const dispatch = useDispatch();
    const [vehicle, setVehicle] = useState(null);
    const [visible, setVisible] = useState(false);
    const [dates, setDates] = useState([]);
    const [startTime, setStartTime] = useState(null);
    const [endTime, setEndTime] = useState(null);
    const [loader, setLoader] = useState(false);
    const [reservedDates, setReservedDates] = useState([]);
    const [price, setPrice] = useState(0);


    const getRentals = async () => {
        try {
            const response = await getAll({vehicleId: id});
            setReservedDates(getRentedDates(response.data.items));
        } catch (e) {
            console.log(e);
        }
    }

    const getVehicle = async () => {
        dispatch(setLoaderState(true));
        try {
            const response = await getOne(id);
            await getRentals();
            setVehicle(response.data);
        } catch (e) {
            console.log(e);
        } finally {
            dispatch(setLoaderState(false));
        }
    }

    useEffect(() => {
        getVehicle();
    }, []);

    useEffect(() => {
        calculatePrice();
    }, [dates]);

    const handleSetDate = (e) => {
        setDates(e.value);
        setStartTime(null);
        setEndTime(null);
    }

    const calculatePrice = () => {
        if (dates && dates[1]) {
            const diffInMilliseconds = dates[1] - dates[0];
            const diffInDays = Math.floor(diffInMilliseconds / (1000 * 60 * 60 * 24)) + 1;
            const finalPrice = diffInDays * vehicle.pricePerDay;
            setPrice(finalPrice);
            return finalPrice;
        }
        return 0;
    }

    const handleSetStartTime = (e) => {
        setStartTime(e.value);
        if (e.value) {
            const d = dateMaker(dates[0], e.value);
            setDates(prevItems => {
                const updatedItems = [...prevItems];
                updatedItems[0] = d;
                return updatedItems;
            });
        }
    }

    const handleSetEndTime = (e) => {
        setEndTime(e.value);
        if (e.value) {
            const d = dateMaker(dates[1], e.value);
            setDates(prevItems => {
                const updatedItems = [...prevItems];
                updatedItems[1] = d;
                return updatedItems;
            });
        }
    }

    const handleSubmit = async () => {
        try {
            const rental = {
                vehicle_id: vehicle.id,
                user_id: user.id,
                start_date: dates[0],
                end_date: dates[1],
                total_cost: price,
                status: "ongoing"
            }
            setLoader(true);
            await add(rental);
        } catch (e) {
            console.log(e);
        } finally {
            setLoader(false);
            setVisible(false);
            setDates(null);
            setStartTime(null);
            setEndTime(null);
            showSuccess(toastRef, "Your reservation was successful");
        }
    }

    const accept = () => {
        handleSubmit();
    };

    const confirm1 = (event) => {
        confirmPopup({
            group: 'headless',
            target: event.currentTarget,
            message: 'Are you sure you want to proceed?',
            icon: 'pi pi-exclamation-triangle',
            defaultFocus: 'accept',
            accept,
        });
    };

    return (
        <div className="bg-gray-100 pb-10">
            <div className={`${styles.main} flex gap-8 p-10`}>
                <div className="w-1/2">
                    <img src={vehicle?.imageUrl} alt=""/>
                </div>
                <div className={`${styles.info} flex flex-col gap-2 w-1/2 p-10`}>
                    <h1 className="font-bold text-3xl p-0 m-0 text-center">{vehicle?.make}</h1>
                    <h2 className="font-bold text-xl p-0 mb-4 text-center text-gray-400">{vehicle?.model}</h2>
                    <div className="flex gap-3 justify-between">
                        <div className="flex flex-col gap-2">
                            <p><span>Color:</span> {vehicle?.color.name}</p>
                            <p><span>Year:</span> {new Date(vehicle?.year).getFullYear() + 1}</p>
                            <p><span>Body type:</span> {vehicle?.type.name}</p>
                            <p><span>Transmission:</span> {vehicle?.transmission.name}</p>
                            <p><span>Miles per gallon(MPG):</span> {vehicle?.mpg} mi</p>
                        </div>
                        <div className="flex flex-col gap-2">
                        <p><span>Seats:</span> {vehicle?.seats}</p>
                            <p><span>Fuel type:</span> {vehicle?.fuelType.name}</p>
                            <p><span>Mileage:</span> {vehicle?.mileage} mi</p>
                            <p><span>Price per day:</span> ${vehicle?.pricePerDay}</p>
                        </div>
                    </div>
                    <div>
                        {/*<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At cum debitis eaque esse est*/}
                        {/*    expedita laudantium officia placeat reiciendis similique. Aut dolorem ipsam ipsum, maiores*/}
                        {/*    nam quae quod repellat vitae.*/}
                        {/*</p>*/}
                    </div>
                    <Button
                        disabled={user === null}
                        label="Rent now"
                        severity="info"
                        onClick={() => setVisible(true)}
                    />
                    {
                        !user &&
                        <span className="text-center text-red-600">In order to rent this vehicle, you have to log in to the system</span>
                    }
                </div>
            </div>
            <Dialog header={`${vehicle?.make} ${vehicle?.model}`} dismissableMask visible={visible}
                    style={{width: '70vw'}} onHide={() => setVisible(false)}
            >
                <div className="card flex-auto justify-content-center">
                    <label htmlFor="calendar-range" className="font-bold block mb-2">
                        Select rent dates
                    </label>
                    <Calendar id="calendar-range"
                              className="w-full"
                              value={dates}
                              onChange={handleSetDate}
                              selectionMode="range"
                              readOnlyInput
                              hideOnRangeSelection
                              minDate={new Date()}
                              disabledDates={reservedDates}
                              selectOtherMonths
                    />
                </div>
                <div className="flex gap-6 mb-4 items-end">
                    <div className="w-full">
                        <label htmlFor="startTime" className="font-bold block mb-2">
                            Start time
                        </label>
                        <Calendar
                            className="w-full"
                            id="startTime"
                            value={startTime}
                            onChange={handleSetStartTime}
                            hideOnDateTimeSelect
                            hourFormat="24"
                            timeOnly/>
                    </div>
                    <div className="w-full">
                        <label htmlFor="endTime" className="font-bold block mb-2">
                            End time
                        </label>
                        <Calendar
                            className="w-full"
                            id="endTime"
                            value={endTime}
                            onChange={handleSetEndTime}
                            hideOnDateTimeSelect
                            hourFormat="24"
                            timeOnly/>
                    </div>
                    <div className="w-full">
                        <p className="font-bold uppercase">Final price is: ${price}</p>
                    </div>
                </div>

                <Button label="Submit" severity="info" onClick={confirm1}/>
                <ConfirmPopup
                    group="headless"
                    content={({message, acceptBtnRef, rejectBtnRef, hide}) =>
                        <div className="bg-gray-900 text-white border-round p-3">
                            <span>{message}</span>
                            <div className="flex align-items-center gap-2 mt-3">
                                <Button loading={loader} ref={acceptBtnRef} label="Yes" onClick={() => {
                                    accept();
                                    hide();
                                }} className="p-button-sm p-button-outlined"></Button>
                                <Button ref={rejectBtnRef} label="Cancel" outlined onClick={() => {
                                    hide();
                                }} className="p-button-sm p-button-text"></Button>
                            </div>
                        </div>
                    }
                />
            </Dialog>
            <Reviews vehicleId={ id } />
        </div>
    )
}