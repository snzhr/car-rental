import styles from './AllVehicles.module.css';
import { VehiclesList } from "../../components/vehicles-list/VehiclesList";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getAll } from "../../../api/vehicle-api";
import { Sort } from "../../../shared/ui/sort/Sort";
import { Pagination } from "../../../shared/ui/pagination/Pagination";
import { Filters } from "../../../shared/ui/filters/Filters";
import { ProgressSpinner } from "primereact/progressspinner";

export const AllVehicles = () => {
    const [vehicles, setVehicles] = useState([]);
    const [paging, setPaging] = useState({
        items: 0,
        page: 1,
        perPage: 10,
    });
    const [query, setQuery] = useState({...paging});
    const [loading, setLoading] = useState(false);

    async function getVehicles(query) {
        try {
            setLoading(true);
            const response = await getAll(query);
            setPaging({...paging, items: response.data.itemsTotal});
            setVehicles(response.data.items);
        } catch (e) {
            console.log(e);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getVehicles(query);
    }, [query]);

    const handleFilter = (options) => {
        setQuery(() => {
            return {
                ...query,
                ...options
            }
        });
    }

    const handleSortData = (sortQuery) => {
        setQuery({...query, ...sortQuery});
    }

    const handlePageChange = (e) => {
        setQuery({...query, page: e.page + 1, perPage: e.rows});
    }

    const handleFormReset = (q) => {
        setQuery({...query, ...q});
    }

    return (
        <div className="flex p-4 gap-4" style={{paddingLeft: '20%'}}>
            <div className={styles.sidebar}>
                <h2 className="text-center font-bold uppercase">Filters</h2>
                <Filters onFilter={handleFilter} onReset={handleFormReset}/>
            </div>
            <div className={styles.main}>
                <Sort onSortData={handleSortData}/>
                {loading ?
                    <div className="card flex justify-content-center items-center h-80">
                        <ProgressSpinner/>
                    </div>
                    : <>
                        {vehicles.length ? <VehiclesList vehicles={vehicles}/> : <h3 className="text-center">No vehicles found</h3>}
                    </>
                }
                <Pagination totalItems={paging.items} onPageClick={handlePageChange}/>
            </div>
        </div>
    )
}