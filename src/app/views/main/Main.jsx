import { Header } from "../../components/header/Header";
import { Outlet } from "react-router-dom";

export const Main = () => {
    return (
        <>
            <Header/>
            <div>
                <Outlet/>
            </div>
        </>
    )
}