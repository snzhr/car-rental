import { useNavigate } from 'react-router-dom';
import './NotFound.css';

function NotFound() {
    const router = useNavigate();
    const handleBackHome = () => {
        router('/');
    }
    return (
        <div className="App">
            <header className="App-header">
                <h1 id= "notFound">
                    404
                </h1>
                <h2>
                    Page Not Found
                </h2>
                <button id="backHomeBtn" onClick={handleBackHome}>Back to Home</button>

            </header>
        </div>
    );
}

export default NotFound;