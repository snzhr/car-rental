import { PageHeader } from "../../../app-admin/components/page-header/PageHeader";
import { CarForm } from "../../../app-admin/components/car-form/CarForm";
import { useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";
import { showSuccess, showWarn } from "../../../shared/notification";
import { toastRef } from "../../../App";
import { upload } from "../../../api/file-api";
import { add, getOne, update } from "../../../api/vehicle-api";
import { vehicleMessages } from "../../../shared/messages";
import { useParams } from "react-router-dom";

export const UpdateHostCar = () => {
    const {user} = useSelector(state => state.user);
    const {id} = useParams();
    const childRef = useRef();
    const [car, setCar] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function fetchVehicle() {
            try {
                const response = await getOne(id);
                setCar(response.data);
            } catch (e) {
                console.log(e);
            }
        }

        fetchVehicle();
    }, []);


    const handleAddCar = async ({file, car}) => {
        try {
            setLoading(true);
            let imageUrl = car.imageUrl;
            if (file){
                imageUrl = await upload(file);
            }
            await update(car.id, {...car, imageUrl});
            showSuccess(toastRef, vehicleMessages['vehicle-updated-successfully']);
        } catch (e) {
            console.log(e);
        }finally {
            setLoading(false);
        }
    }
    return (
        <div className="p-10">
            <PageHeader header="Host: update vehicle"/>
            <CarForm onSave={handleAddCar} vehicle={car} loader={loading} user={user} ref={childRef}/>
        </div>
    )
}