import {useRef, useState} from "react";
import styles from "../register/Register.module.css";
import {InputText} from "primereact/inputtext";
import {Password} from "primereact/password";
import {Button} from "primereact/button";
import { Link, useNavigate } from "react-router-dom";
import { signup } from "../../../api/auth-api";
import {Toast} from "primereact/toast";
import { showError } from "../../../shared/notification";
import { messages } from "../../../shared/messages";
import { setToken } from "../../../store/slices/userSlice";
import { useDispatch } from "react-redux";

export const Register = () => {
    const toast = useRef(null);
    const [user, setUser] = useState({email: '', password: "", name: "", lastname: ""});
    const router = useNavigate();
    const dispatch = useDispatch();

    function handleInputChange(e) {
        const { name, value } = e.target;
        setUser({
            ...user,
            [name]: value,
        });
    }
    async function handleFormSubmit(e){
        e.preventDefault();
        try{
            const res = await signup(user);
            localStorage.setItem("token", res.data.authToken);
            dispatch(setToken(res.data.authToken));
            router("/");
        }catch (e) {
            showError(toast, messages[e.code]);
        }
    }

    return (
        <div className={styles.content}>
            <Toast ref={toast}/>
            <div className={styles['form-wrapper']}>
                <form className="flex flex-col justify-center items-center" onSubmit={(e) => handleFormSubmit(e)}>
                    <div className="p-inputgroup">
                        <span className="p-inputgroup-addon">
                            <i className="pi pi-user"></i>
                        </span>
                        <InputText name="name" value={user.name} placeholder="Name" onChange={handleInputChange}/>
                    </div>
                    <div className="p-inputgroup">
                        <span className="p-inputgroup-addon">
                            <i className="pi pi-user"></i>
                        </span>
                        <InputText name="lastname" value={user.lastname} placeholder="Lastname" onChange={handleInputChange}/>
                    </div>
                    <div className="p-inputgroup">
                        <span className="p-inputgroup-addon">
                            <i className="pi pi-envelope"></i>
                        </span>
                        <InputText name="email" value={user.email} placeholder="Email" onChange={handleInputChange}/>
                    </div>
                    <div className="p-inputgroup">
                        <span className="p-inputgroup-addon">
                            <i className="pi pi-lock"></i>
                        </span>
                        <Password name="password" value={user.password} placeholder="Password" toggleMask
                                  onChange={handleInputChange}/>
                    </div>
                    <Button type="submit" label="Register" severity="secondary" raised className="w-2/3"/>
                    <span>Don't have an account? <Link to="/login">Sign in</Link></span>
                    <Link className={styles['password-reset']}>Forgot password?</Link>
                </form>
            </div>
        </div>
    )
}