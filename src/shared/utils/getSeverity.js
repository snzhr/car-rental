export const getSeverity = (status) => {
    switch (status) {
        case 'returned':
            return 'success';
        case 'ongoing':
            return 'warning';
        case 'cancelled':
            return 'danger';
        default:
            return null;
    }
};