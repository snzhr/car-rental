export function dateMaker (date1, date2) {
// Extract date part from date1
    const year = date1?.getFullYear();
    const month = date1?.getMonth();
    const day = date1?.getDate();

// Extract time part from date2
    const hours = date2?.getHours();
    const minutes = date2?.getMinutes();
    const seconds = date2?.getSeconds();

// Create a new date object with combined date and time
    const combinedDate = new Date(year, month, day, hours, minutes, seconds);
    return combinedDate;
}

export function getDateRange(startDate, endDate) {
    let dates = [];
    let currentDate = new Date(startDate);

    while (currentDate <= endDate) {
        dates.push(new Date(currentDate));
        currentDate.setDate(currentDate.getDate() + 1);
    }

    return dates;
}

export function getRentedDates(dates){
    const localdates = [];
    dates.forEach(item => {
        const start = item.start_date?.split('-').join('/');
        const end = item.end_date?.split('-').join('/');
        const dateRange = getDateRange(new Date(start), new Date(end));
        localdates.push(dateRange);
    });

    return localdates.flat();
}