export function objectFlattener(object) {
    let newObj = {};
    for (const queryKey in object) {
        if (object[queryKey]?.name) {
            newObj[queryKey] = object[queryKey].name;
        }else{
            newObj[queryKey] = object[queryKey];
        }
    }
    return newObj;
}