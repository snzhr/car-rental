export const fuel = [
    {name: "Gasoline"},
    {name: "Diesel"},
    {name: "Electric"},
    {name: "Hybrid"},
    {name: "Plug in hybrid"},
    {name: "Flex_fuel"},
    {name: "Compressed_natural_gas"}
];

export const usaStates = [
    {name: 'Alabama'},
    {name: 'Alaska'},
    {name: 'Arizona'},
    {name: 'Arkansas'},
    {name: 'California'},
    {name: 'Colorado'},
    {name: 'Connecticut'},
    {name: 'Delaware'},
    {name: 'Florida'},
    {name: 'Georgia'},
    {name: 'Hawaii'},
    {name: 'Idaho'},
    {name: 'Illinois'},
    {name: 'Indiana'},
    {name: 'Iowa'},
    {name: 'Kansas'},
    {name: 'Kentucky'},
    {name: 'Louisiana'},
    {name: 'Maine'},
    {name: 'Maryland'},
    {name: 'Massachusetts'},
    {name: 'Michigan'},
    {name: 'Minnesota'},
    {name: 'Mississippi'},
    {name: 'Missouri'},
    {name: 'Montana'},
    {name: 'Nebraska'},
    {name: 'Nevada'},
    {name: 'New Hampshire'},
    {name: 'New Jersey'},
    {name: 'New Mexico'},
    {name: 'New York'},
    {name: 'North Carolina'},
    {name: 'North Dakota'},
    {name: 'Ohio'},
    {name: 'Oklahoma'},
    {name: 'Oregon'},
    {name: 'Pennsylvania'},
    {name: 'Rhode Island'},
    {name: 'South Carolina'},
    {name: 'South Dakota'},
    {name: 'Tennessee'},
    {name: 'Texas'},
    {name: 'Utah'},
    {name: 'Vermont'},
    {name: 'Virginia'},
    {name: 'Washington'},
    {name: 'West Virginia'},
    {name: 'Wisconsin'},
    {name: 'Wyoming'}
];

export const carType = [
    {"name": "Sedan"},
    {"name": "SUV"},
    {"name": "Truck"},
    {"name": "Hatchback"},
    {"name": "Convertible"},
    {"name": "Van"},
    {"name": "Coupe"},
    {"name": "Minivan"},
    {"name": "Crossover"},
    {"name": "Wagon"}
];

export const transmission = [
    {"name": "Automatic"},
    {"name": "Manual"},
    {"name": "CVT (Continuously Variable Transmission)"},
    {"name": "Semi-automatic"},
    {"name": "Dual-clutch"}
];

export const colors = [
    {name: "Black"},
    {name: "White"},
    {name: "Silver"},
    {name: "Gray"},
    {name: "Red"},
    {name: "Blue"},
    {name: "Green"},
    {name: "Brown"},
    {name: "Yellow"},
    {name: "Orange"},
    {name: "Beige"},
    {name: "Purple"},
    {name: "Gold"},
    {name: "Bronze"},
    {name: "Charcoal"},
    {name: "Maroon"},
    {name: "Navy"},
    {name: "Cream"},
    {name: "Turquoise"},
    {name: "Teal"}
];

export const sort = [
    {name: "pricePerDay", label: 'Price'},
    {name: "mileage", label: 'Mileage'}
]