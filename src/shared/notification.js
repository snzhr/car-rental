
const showSuccess = (toast, message, life = 3000) => {
    toast.current.show({severity: 'success', summary: 'Success', detail: message, life});
}

const showInfo = (toast, message, life = 3000) => {
    toast.current.show({severity: 'info', summary: 'Info', detail: message, life});
}

const showWarn = (toast, message, life = 3000) => {
    toast.current.show({severity: 'warn', summary: 'Warning', detail: message, life});
}

const showError = (toast, message, life = 3000) => {
    toast.current.show({severity: 'error', summary: 'Error', detail: message, life});
}

export { showError, showInfo, showWarn, showSuccess };