import styles from './Loader.module.css';
import { ProgressSpinner } from "primereact/progressspinner";

export const Loader = () => {
    return (
        <div className={styles.loader}>
            <ProgressSpinner style={{ width: 200, height: 200 }} />
        </div>
    )
}