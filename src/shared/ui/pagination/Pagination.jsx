import { Dropdown } from "primereact/dropdown";
import { useState } from "react";
import { Paginator } from "primereact/paginator";

export const Pagination = ({totalItems, onPageClick, customRows, customOptions}) => {
    const [first, setFirst] = useState([0, 0, 0]);
    const [rows, setRows] = useState(customRows || [10, 10, 10]);

    const template2 = {
        layout: 'RowsPerPageDropdown PrevPageLink CurrentPageReport NextPageLink',
        RowsPerPageDropdown: (options) => {
            const dropdownOptions =
                [
                    {label: 10, value: 10},
                    {label: 20, value: 20},
                    {label: 30, value: 30},
                ];

            return (
                <div>
                    <span style={{color: 'var(--text-color)', userSelect: 'none'}}>
                        Items per page:{' '}
                    </span>
                    <Dropdown
                        value={options.value}
                        options={ customOptions|| dropdownOptions}
                        onChange={options.onChange}
                    />
                </div>
            );
        },
        CurrentPageReport: (options) => {
            return (
                <span style={{color: 'var(--text-color)', userSelect: 'none', width: '120px', textAlign: 'center'}}>
                    {options.first} - {options.last} of {options.totalRecords}
                </span>
            );
        }
    };


    const onPageChange = (e, index) => {
        setFirst(first.map((f, i) => (index === i ? e.first : f)));
        setRows(rows.map((r, i) => (index === i ? e.rows : r)));
        onPageClick(e);
    };

    return (
        <div className="mt-4">
            <Paginator
                template={template2}
                first={first[1]}
                rows={rows[1]}
                totalRecords={totalItems}
                onPageChange={(e) => onPageChange(e, 1)}
                className="justify-content-end"/>
        </div>
    )
}