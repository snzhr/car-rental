import { Dropdown } from "primereact/dropdown";
import { sort } from "../../static";
import { useState } from "react";
import { objectFlattener } from "../../utils/objectFlattener";

export const Sort = ({onSortData}) => {
    const [sorting, setSorting] = useState({
        orderBy: "desc",
        sortBy: {}
    });

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        const q= {...sorting, [name]: value};
        setSorting(() => q);
        const sortQuery = objectFlattener(q);
        onSortData(sortQuery);
    }

    const handleOrderClick = () => {
        let order = sorting?.orderBy;
        order = (order === "desc") ? "asc" : "desc";
        const q= {...sorting, orderBy: order};
        setSorting(q);
        const sortQuery = objectFlattener(q);
        onSortData(sortQuery);
    }


    return (
        <form className="flex items-end justify-end gap-4 mb-5">
            <button type="button" onClick={handleOrderClick}>
                <span style={{fontSize: '30px'}} className={`pi pi-sort-amount-${sorting?.orderBy === 'asc' ? 'up' : 'down'}`}></span>
            </button>

            <div className="card w-1/3">
                        <span className="flex-auto">
                            <label htmlFor="sortBy" className="font-bold block mb-1 uppercase">Sort by:</label>
                            <Dropdown
                                inputId="sortBy"
                                name="sortBy"
                                value={sorting?.sortBy}
                                onChange={handleInputChange}
                                options={sort}
                                optionLabel="label"
                                className="w-full"/>
                        </span>
            </div>
        </form>
    )
}