import styles from './CarCard.module.css';
import { Button } from "primereact/button";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export const    CarCard = ({car, handleEdit, handleRentClick, handleDeleteClick}) => {
    const user = useSelector((state) => state.user.user);
    const {make, model, imageUrl, year, type, transmission, seats, fuelType, pricePerDay, mileage, mpg} = car;
    const location = useLocation();
    const handleEditClick = () => {
        handleEdit(car.id);
    }

    const handleButtonClick = () => {
        if (location.pathname.includes('admin')){
            return
        }
        handleRentClick(car.id);
    }

    const handleRemoveClick = () => {
        handleDeleteClick(car.id);
    }

    return (
        <div className={styles.car}>
            <div className={styles.carImage}>
                {handleEdit &&
                    <div className={styles.edit}>
                        <button onClick={handleEditClick} className="py-0.5 px-2 rounded">
                            <i className="pi pi-pencil"></i>
                        </button>
                        <button onClick={handleRemoveClick} className="py-0.5 px-2 rounded">
                            <i className="pi pi-trash"></i>
                        </button>
                    </div>
                }
                <img onClick={handleButtonClick} src={imageUrl} alt="car image"/>
                <span className={styles.year}>{year.slice(0, 4)}</span>
            </div>
            <div className="mb-5">
                <span className={styles.model}>{model}</span>
                <div className="flex gap-3 text-lg items-center mb-2">
                    <h2 className="font-bold">{make}</h2>
                    {
                        (user && user?.isAdmin === true && user?.id !== car.host_id) &&
                        <h2 className="font-bold text-amber-500">(Host's vehicle)</h2>
                    }
                </div>
                <div className={styles.info}>
                <Button unstyled iconPos="left" icon="pi pi-car" tooltip="Vehicle type" tooltipOptions={{position: "bottom"}} label={type.name}/>
                    <Button unstyled iconPos="left" icon="pi pi-cog" tooltip="Transmission type" tooltipOptions={{position: "bottom"}} label={transmission.name}/>
                    <Button unstyled iconPos="left" icon="pi pi-user" tooltip="Seats" tooltipOptions={{position: "bottom"}} label={seats}/>
                    <Button unstyled iconPos="left" icon="pi pi-filter" tooltip="Fuel type" tooltipOptions={{position: "bottom"}} label={fuelType.name}/>
                    <Button unstyled iconPos="left" icon="pi pi-gauge" tooltip="Mileage" tooltipOptions={{position: "bottom"}} label={mileage}/>
                    <Button unstyled iconPos="left" icon="pi pi-bolt" tooltip="Miles per gallon" tooltipOptions={{position: "bottom"}} label={mpg || "0"}/>
                </div>
            </div>
            <div className="flex justify-between items-center">
                {
                !user?.isAdmin && <Button onClick={handleButtonClick} label="Rent now" severity="primary" rounded/>
                }
                    <p>
                    <span className="font-bold text-xl">${pricePerDay}</span>
                    <span className="font-bold">/day</span>
                </p>
            </div>
        </div>
    )
}