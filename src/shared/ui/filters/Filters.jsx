import { Dropdown } from "primereact/dropdown";
import { carType, colors, fuel, transmission } from "../../static";
import { InputNumber } from "primereact/inputnumber";
import { Button } from "primereact/button";
import { objectFlattener } from "../../utils/objectFlattener";
import { useEffect, useState } from "react";

export const Filters = ({onFilter, onReset}) => {
    const [filters, setFilters] = useState({
        year: '',
        color: null,
        type: null,
        seats: '',
        transmission: null,
        fuelType: null,
        mpgMin: 0,
        mpgMax: 0,
        priceMin: 0,
        priceMax: 0
    });

    useEffect(() => {
        setFilters({...filters, priceMin: 0, priceMax: 0})
    }, []);

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setFilters({
            ...filters, [name]: value,
        });
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        const q = objectFlattener(filters);
        onFilter(q);
    }

    const handleFormReset = (e) => {
        e.preventDefault();
        const emptyFilters = {
            year: '',
            color: null,
            type: null,
            seats: '',
            transmission: null,
            fuelType: null,
            mpgMin: 0,
            mpgMax: 0,
            priceMin: 0,
            priceMax: 0
        }
        setFilters(emptyFilters);
        onReset(emptyFilters);
    }

    return (
        <form className="flex flex-col gap-4" onSubmit={handleFormSubmit} onReset={handleFormReset}>
            <div className="card w-full">
                        <span className="flex-auto">
                            <label htmlFor="color" className="font-bold block mb-1">Color</label>
                            <Dropdown
                                inputId="color"
                                showClear
                                name="color"
                                value={filters.color}
                                onChange={handleInputChange}
                                options={colors}
                                optionLabel="name"
                                className="w-full"/>
                        </span>
            </div>
            <div className="card w-full">
                <span className="flex-auto">
                <label htmlFor="type" className="font-bold block mb-1">Type</label>
                      <Dropdown
                          inputId="type"
                          name="type"
                          showClear
                          value={filters.type}
                          onChange={handleInputChange}
                          options={carType}
                          optionLabel="name"
                          className="w-full"/>
                </span>
            </div>
            <div className="card w-full">
                <span className="flex-auto">
                <label htmlFor="transmission" className="font-bold block mb-1">Transmission</label>
                    <Dropdown
                        inputId="transmission"
                        name="transmission"
                        showClear
                        value={filters.transmission}
                        onChange={handleInputChange}
                        options={transmission}
                        optionLabel="name"
                        className="w-full"/>
                </span>
            </div>
            <div className="card w-full">
                        <span className="flex-auto">
                            <label htmlFor="fuelType" className="font-bold block mb-1">Fuel type</label>
                            <Dropdown
                                inputId="fuelType"
                                name="fuelType"
                                showClear
                                value={filters.fuelType}
                                onChange={handleInputChange}
                                options={fuel}
                                optionLabel="name"
                                className="w-full"/>
                        </span>
            </div>
            <div className="card w-full">
                <span className="flex-auto">
                    <label htmlFor="seats" className="font-bold block mb-1">Car seats</label>
                    <InputNumber className="w-full"
                                 inputId="seats"
                                 name="seats"
                                 value={filters.seats}
                                 onValueChange={handleInputChange}
                                 mode="decimal"
                                 showButtons
                                 min={2}
                                 max={7}/>
                </span>
            </div>
            <div className="card w-full">
                        <span className="flex-auto">
                            <label htmlFor="mpgMin" className="font-bold block mb-1">MPG min</label>
                            <InputNumber className="w-full"
                                         inputId="mpgMin"
                                         name="mpgMin"
                                         value={filters.mpgMin}
                                         onValueChange={handleInputChange}
                                         mode="decimal"
                                         showButtons
                                         min={0}
                                         max={100}/>
                        </span>
            </div>
            <div className="card w-full">
                        <span className="flex-auto">
                            <label htmlFor="mpgMax" className="font-bold block mb-1">MPG max</label>
                            <InputNumber className="w-full"
                                         inputId="mpgMax"
                                         name="mpgMax"
                                         value={filters.mpgMax}
                                         onValueChange={handleInputChange}
                                         mode="decimal"
                                         showButtons
                                         min={0}
                                         max={100}/>
                        </span>
            </div>
            <div className="card w-full">
                        <span className="flex-auto">
                            <label htmlFor="priceMin" className="font-bold block mb-1">Price min</label>
                            <InputNumber className="w-full"
                                         inputId="priceMin"
                                         name="priceMin"
                                         value={filters.priceMin}
                                         onValueChange={handleInputChange}
                                         mode="decimal"
                                         showButtons
                                         min={0}
                                         max={1000}/>
                        </span>
            </div>
            <div className="card w-full">
                        <span className="flex-auto">
                            <label htmlFor="priceMax" className="font-bold block mb-1">Price max</label>
                            <InputNumber className="w-full"
                                         inputId="priceMax"
                                         name="priceMax"
                                         value={filters.priceMax}
                                         onValueChange={handleInputChange}
                                         mode="decimal"
                                         showButtons
                                         min={0}
                                         max={1000}/>
                        </span>
            </div>
            <Button label="Apply" type="submit" severity="info"/>
            <Button label="Reset" type="reset" severity="secondary"/>
        </form>
    )
}