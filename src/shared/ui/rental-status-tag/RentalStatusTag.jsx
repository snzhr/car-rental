import { Tag } from "primereact/tag";
import { getSeverity } from "../../utils/getSeverity";

export const RentalStatusTag = ({status}) => {
    return (
        <>
            <Tag value={status} className="text-white uppercase" severity={getSeverity(status)}></Tag>
        </>
    )
}