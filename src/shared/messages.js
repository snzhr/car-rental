export const messages = {
    "auth/invalid-email": "Invalid email provided.",
    "auth/email-already-exists": "This email already exists.",
    "auth/invalid-password": "Provided password is incorrect.",
    "auth/user-not-found": "User is not found.",
    "auth/invalid-credential": "Provided email or password is invalid"
}

export const formMessages = {
    "provide-old-password": "Please provide old password",
    "password-not-match": "Passwords didn't match",
    "password-success": "Password successfully changed"
}

export const vehicleMessages = {
    'vehicle-added-successfully': "Vehicle was added successfully",
    'vehicle-updated-successfully': "Vehicle was updated successfully"
}

export const userMessages = {
    "user-updated": "User was successfully updated",
}

