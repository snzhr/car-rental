import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";
import {getAuth} from "firebase/auth";
// import { getAnalytics } from "firebase/analytics";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyDEFCADIyz1SozPqnZA5j9hWsLqGHZrr8Q",
    authDomain: "car-rental-b989d.firebaseapp.com",
    projectId: "car-rental-b989d",
    storageBucket: "car-rental-b989d.appspot.com",
    messagingSenderId: "896505050143",
    appId: "1:896505050143:web:4414ce5c1191954d62d12a",
    measurementId: "G-6GQ02ZKNMN"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
export const db = getFirestore(app);
export const auth = getAuth(app);

export const storage = getStorage(app);
